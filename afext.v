(* Some extensions to Almost Full
   By Mitsuharu Yamamoto and Saki Matsumoto.  *)

From AlmostFull.PropBounded Require Import AlmostFull AFConstructions.
From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Section AlmostFullExtension.

Lemma af_leq : almost_full leq.
Proof.
  by apply: (af_strengthen leq_af) => x y /leP.
Qed.

Lemma af_unit : almost_full (@eq unit). 
Proof. 
  apply: AF_ZT. case. by case.
Qed.

Section Option.

Variable X : Type.
Variable A : X -> X -> Prop.

Definition option_lift (x y : option X) :=
  match x, y with
    | Some x0, Some y0 => A x0 y0
    | None, None => True
    | _, _ => False
  end.

Lemma af_option : almost_full A -> almost_full option_lift.
Proof.
  pose option_to_sum (x : option X) := if x is Some x0 then inl x0 else inr tt.
  move=> afA.
  apply: (af_strengthen (af_cofmap option_to_sum (af_sum_lift afA af_unit))).
  by do !case => [ ? | ].
Qed.

End Option.


Section Tuple_to_ffun.

Variable aT : finType.
Variable rT : Type.
Variable A : rT -> rT -> Prop.

Definition finfun_lift := fun (x y : {ffun aT -> rT}) => forall a : aT, A (x a) (y a).  

Definition tuple_lift n :=
  fun (x y : n.-tuple rT) => forall i : 'I_n, A (tnth x i) (tnth y i).

Definition k_tuple_lift n k :=
  fun (x y : n.-tuple rT) =>
    forall i : 'I_n, i < k -> A (tnth x i) (tnth y i).

Lemma af_tuplek : almost_full A ->
                  forall n, forall k, almost_full (@k_tuple_lift n k).
 Proof.
  move => afA n.
  elim; first by exact: AF_ZT.
  move => k af_ltk.
  case: (ltnP k n) => H.
  - have af_k :
      almost_full (fun (x y : n.-tuple rT) => A (tnth x (Ordinal H)) (tnth y (Ordinal H)))
    by apply: af_cofmap.
    apply: (af_strengthen (af_intersection af_k af_ltk)) => x y []  Hk Htupk i.
    rewrite ltnS leq_eqVlt => /predU1P -[Hik |]; last by move /(Htupk i).
    suff ->: i = Ordinal H by [].
    by apply: ord_inj.
  - apply: (af_strengthen af_ltk) => x y Htupk i _.
    by apply: Htupk; apply: leq_trans H.
Qed.

Lemma af_tuple n : almost_full A -> almost_full (@tuple_lift n).
Proof. 
  move => afA.
  apply: (af_strengthen (af_tuplek afA n n)) => x y Hktupn i.
  by move: (Hktupn i (ltn_ord i)).
Qed. 

Lemma af_finfun : almost_full A -> almost_full finfun_lift. 
Proof.
  move /af_tuple.
  rewrite /finfun_lift => af_tuple.
  apply: (af_strengthen (af_cofmap (@fgraph aT rT) (af_tuple #|aT|))) => x y. 
  rewrite /tuple_lift => H a.
  by rewrite -[a]enum_rankK -!tnth_fgraph.  
Qed.  

End Tuple_to_ffun.

End AlmostFullExtension.
