(* Karp-Miller tree on Petri nets.
   Tree construction and coverability by Mitsuharu Yamamoto and Saki Matsumoto.
   Boundedness and termination by Mitsuharu Yamamoto and Mamoru Inagaki.  *)

Require Import Relations.
From AlmostFull.PropBounded Require Import AlmostFull AFConstructions.
From mathcomp Require Import all_ssreflect.
Import Order.TTheory.
Require Import ssreflectext orderext monad afext petrinet pnkmaccel.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

(* From Program/Utils.v*)
Notation dec := Sumbool.sumbool_of_bool.

Lemma dec_op (X : Type) (x : option X) : {x' | x = Some x'} + {x = None}.
Proof. by case: x => [x | ]; [left; exists x | right]. Defined.

Section KarpMillerTree.

Variable pn : petri_net.

Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).

Section MakeKarpMillerTree.

Definition mkkmt_inv (mcs : seq markingc) (mc : markingc) : bool :=
  all (fun mc' => topsmc mc' \subset topsmc mc) mcs.

Lemma mkkmt_inv_nextmc (mcs1 : seq markingc) (mc1 mc2: markingc) t :
  mc1 =1{` t `}> mc2 -> mkkmt_inv mcs1 mc1 -> mkkmt_inv (rcons mcs1 mc1) mc2.
Proof.
  move/topsmc_nextmc=> Htops Hinv; apply/allP=> /= mc; rewrite Htops.
  by rewrite mem_rcons inE => /predU1P [->|] // Hin; rewrite (allP Hinv).
Qed.

Lemma mkkmt_inv_subset_topsmc (mcs : seq markingc) (mc1 mc2: markingc) :
  topsmc mc1 \subset topsmc mc2 -> mkkmt_inv mcs mc1 -> mkkmt_inv mcs mc2.
Proof.
  move=> Hsub Hinv; apply/allP=> /= mc Hin; apply: subset_trans Hsub.
  by rewrite (allP Hinv).
Qed.

Lemma mkkmt_inv_nextma (mcp : {mcp | mkkmt_inv mcp.1 mcp.2}) mcp' t :
  (val mcp) =1{! t !}> mcp' -> mkkmt_inv mcp'.1 mcp'.2.
Proof. 
  move: mcp mcp' => [[mcs mc] /= Hinv] [mcs' mc'] /=.
  case: nextmaP => //= mc0 Ht [<- <-].
  apply: (mkkmt_inv_subset_topsmc (topsmc_accel _ _)).
  exact: (mkkmt_inv_nextmc Ht).
Qed.

Definition mkkmtreeT (mcp' mcp : {mcp | mkkmt_inv mcp.1 mcp.2}) :=
  (exists t, (val mcp) =1{! t !}> (val mcp')) /\
  (val mcp').2 \notin (val mcp').1.

Lemma mkkmtreeT_next (mcp : {mcp | mkkmt_inv mcp.1 mcp.2}) mcp' t :
  forall Hnext : (val mcp) =1{! t !}> mcp',
  (mcp'.2 \in mcp'.1) = false ->
  mkkmtreeT (Sub mcp' (mkkmt_inv_nextma Hnext)) mcp.
Proof.
  move=> Hnext Hnotin.
  split; first by exists t.
  by rewrite Hnotin.
Qed.

Definition fincovermc (mc mc' : markingc) : bool :=
  (mc <= mc') && (topsmc mc == topsmc mc').

Lemma fincovermcP (mc mc' : markingc) :
  reflect (finfun_lift (option_lift leq) mc mc')
          (fincovermc mc mc').
Proof.
  apply /(iffP andP).
  - case.
    move /forallP => H1 H2 p.
    case Hmc: (mc p)  (H1 p) => [n |]; case Hmc': (mc' p) => [m |] //=.
    move: H2.
    move /eqP /setP /(_ p).
    by rewrite !inE Hmc Hmc'.
  - move => H.
    split.
    + apply /forallP => p.
      case: (mc p) (H p) => [n |]; case: (mc' p) => [m |] //=.
    + apply /eqP /setP => p.
      move: (H p).
      rewrite !inE.
      by case: (mc p) => [n |]; case: (mc' p) => [m |] //=.
Qed.

Definition mkkmtreeR (mcp mcp' : {mcp | mkkmt_inv mcp.1 mcp.2}) :=
  fincovermc (val mcp).2 (val mcp').2.

Lemma af_mkkmtreeR : almost_full mkkmtreeR.
Proof.
  pose f := fun (mcp : {mcp | mkkmt_inv mcp.1 mcp.2}) => (val mcp).2.
  apply: (@af_cofmap _ _ f fincovermc).
  apply: (@af_strengthen _ (finfun_lift (option_lift leq))).
  - by apply: af_finfun; apply: af_option; apply: af_leq.
  - by move => mc mc' /fincovermcP.
Qed.

Lemma wf_lem0 mc mc' mc0 mcs : 
  mkkmt_inv mcs mc0 -> fincovermc mc mc' ->
  accelmc mcs mc0 = mc' -> mc \in mcs -> mc <= mc0.
Proof.
  move/allP=> /= Hinv /andP [H1 /eqP H2] Hacc Hin.
  apply/forallP => p.
  move /forallP /(_ p): H1.
  move /setP /(_ p): H2.
  rewrite !inE.
  move /ffunP /(_ p): Hacc.
  case: accelmcP; [by move=> n _ -> <-; case: (mc p) | | by move=> ->].
  move=> n _ _ _ _ Hmc' <-.
  move/(_ _ Hin)/subsetP/(_ p): Hinv.
  rewrite !inE Hmc'.
  by case: (mc p) => // /(_ isT) /eqP.
Qed.

Lemma wf_lem mc mc' mc0 mcs : 
  mkkmt_inv mcs mc0 -> fincovermc mc mc' ->
  accelmc mcs mc0 = mc' -> mc \in mcs -> mc = mc'.
Proof.
  move => Hinv Hcover Hacc Hin.
  have Hleq: (mc <= mc0) by apply: wf_lem0 Hacc _.
  apply: le_anti.
  move /andP :Hcover => [-> /eqP Htops] /=.
  apply /forallP => p.
  move /ffunP /(_ p): Hacc.
  move /setP /(_ p): Htops.
  rewrite !inE.
  case Hmc: (mc p) => [n |]; case Hmc': (mc' p) => [n' |] => //= Hnn'.
  case: accelmcP => //.
  move => n'' H Hmc0 [] Hn''n'.
  move: (H _ Hin Hleq).
  by rewrite Hmc0 Hmc -leqNgt Hn''n'. 
Qed.

Lemma wf_lem' (mcp' mcp : {mcp | mkkmt_inv mcp.1 mcp.2}) : 
  clos_trans_1n _ mkkmtreeT mcp' mcp -> (val mcp).2 \in (val mcp').1.
Proof.
  elim: mcp' mcp /.
  - move=> mcp' mcp [[t]].
    case: nextmaP => //= _ _ [<- _] /=.
    by rewrite mem_rcons inE eqxx.
  - move=> mcp' mcp mcp0 [[t]].
    case: nextmaP => //= _ _ [<- _] /= _ Hin.
    by rewrite mem_rcons inE Hin orbT.
Qed.

Lemma wf_mkkmtreeT : well_founded mkkmtreeT.
Proof.
  apply: wf_from_af; last by apply: af_mkkmtreeR.
  move=> [[mcs' mc'] /= Hinv'] [[mcs mc] /= Hinv] [] Hclos.
  have [[[mcs0 mc0] /= Hinv0] [/= [t Ht] Hnotin]]:
    exists mcp0, mkkmtreeT (Sub (mcs', mc') Hinv') mcp0
      by case: Hclos => [mcp0 HmktreeT | mcp0 _ HmktreeT _]; exists mcp0.
  move/wf_lem': Hclos => /= Hin.
  case: nextmaP Ht=> //= mc0' Hnext [Hrcons Hacc] HR.
  suff Heq: mc = mc' by move: Hnotin; rewrite -Heq Hin.
  rewrite Hrcons in Hacc.
  apply: wf_lem Hacc Hin => //.
  rewrite -Hrcons.
  exact: (mkkmt_inv_nextmc Hnext).
Qed.

(* For mathcomp < 1.8.0:
Inductive kmtree :=
  DLf | Lf of markingc | Br of markingc & (transition -> kmtree).
*)
Unset Elimination Schemes.
Inductive kmtree :=
  DLf | Lf of markingc | Br of markingc & {ffun transition -> kmtree}.
Lemma kmtree_ind P (PDLf : P DLf) (PLf : forall m, P (Lf m))
      (PBr: forall m (f : {ffun transition -> kmtree}),
          (forall t, P (f t)) -> P (Br m f)) : forall t, P t.
Proof.
  fix IHf 1 => -[||m f]; [exact: PDLf|exact: PLf|].
  by apply:PBr => t; apply: IHf.
Qed.
Set Elimination Schemes.

Definition mkkmtreeF (mcp : {mcp | mkkmt_inv mcp.1 mcp.2})
           (F' : forall y, mkkmtreeT y mcp -> kmtree) :=
  Br (val mcp).2 [ffun t : transition => 
                  match dec_op (nextma (val mcp) t) with
                  | inleft (exist mcp' Hnext) => 
                    match dec (mcp'.2 \in mcp'.1) with
                    | left _ => Lf mcp'.2
                    | right Hnotin => F' (Sub mcp' (mkkmt_inv_nextma Hnext)) 
                                         (mkkmtreeT_next Hnext Hnotin)
                    end
                  | inright _ => DLf
                  end].

Definition mkkmtree := Fix wf_mkkmtreeT _ mkkmtreeF.

Lemma mkkmtreeE mcp :
  mkkmtree mcp =
  Br (val mcp).2 [ffun t : transition =>
                  match dec_op (nextma (val mcp) t) with
                  | inleft (exist mcp' Hnext) =>
                    if mcp'.2 \in mcp'.1
                    then Lf mcp'.2
                    else mkkmtree (Sub mcp' (mkkmt_inv_nextma Hnext))
                  | inright _ => DLf
                  end].
Proof.
  rewrite /mkkmtree Fix_eq.
  - congr Br; apply/ffunP => t; rewrite !ffunE.
    by case: dec_op => // [[mcp' Hnext]]; case: ifP.
  - move=> mcp0 f g H.
    congr Br; apply/ffunP => t; rewrite !ffunE.
    by case: dec_op => // [[mcp' Hnext]]; case: dec.
Qed.

End MakeKarpMillerTree.


Fixpoint in_kmtree tree (mc0 : markingc) :=
  if tree is Br mc f then (mc0 == mc) || [exists t, in_kmtree (f t) mc0]
  else false.
Canonical kmtree_predType := PredType in_kmtree.

Lemma in_Br (mc mc0 : markingc) f : (mc0 \in Br mc f) = 
                                    (mc0 == mc) || [exists t, mc0 \in f t].
Proof. by []. Qed.

Lemma in_Lf (mc mc0 : markingc) : (mc0 \in Lf mc) = false.
Proof. by []. Qed.

Lemma in_DLf (mc0 : markingc) : (mc0 \in DLf) = false.
Proof. by []. Qed.

Definition inE := (in_Br, in_Lf, in_DLf, inE).

Lemma in_mkkmtreeP (mcp : {mcp | mkkmt_inv mcp.1 mcp.2}) mc:
  reflect (exists s mcs,
              (val mcp) ={! s !}> (mcs, mc) /\ uniq (rcons mcs mc))
          ((mc \in mkkmtree mcp) && uniq (rcons (val mcp).1 (val mcp).2)).
Proof.
  apply (iffP andP).
  - case; move: mcp.
    apply: (well_founded_induction wf_mkkmtreeT) => mcp Hind Htree Huniq.
    move: Htree; rewrite mkkmtreeE.
    case/predU1P; first by move => ->; exists [::], (val mcp).1; rewrite -surjective_pairing.
    case/existsP=> t; rewrite ffunE.
    case: dec_op => // -[mcp' Ht]; case: ifP => // Hnotin.
    case/(Hind (Sub mcp' (mkkmt_inv_nextma _)) (mkkmtreeT_next _ _)) => //=.
    + by rewrite rcons_uniq Hnotin /=; case: nextmaP Ht => // mc' _ [<-].
    + move=> s [mcp'' [Hs Huniq']]; exists (t :: s), mcp''; split => //.
      by rewrite foldm_cons_some; exists mcp'.
  - case => s [mcp'].
    elim: s => [| t s IHs] in mcp mcp' *.
    + rewrite [val mcp]surjective_pairing => -[[<- <-] ->].
      by split; first by rewrite mkkmtreeE inE eqxx.
    + case=> Hts Huniq.
      split; last by case: (nextma_seq_extend Hts) Huniq=> mcs ->; rewrite cat_uniq; case: uniq.
      rewrite mkkmtreeE; apply /orP; right.
      apply /existsP; exists t; rewrite ffunE.
      move: Hts.
      rewrite foldm_cons_some => -[mcp'' [Ht Hs]].
      case: dec_op => [[mcp0 Ht']| ]; last by rewrite Ht.
      case: ifP => [/= | Hnotin].
      * move: Ht'; rewrite Ht => -[] <- => Hin.
        case: (nextma_seq_extend Hs) Huniq => mcs ->.
        by rewrite cat_uniq rcons_uniq Hin.
      * apply: (IHs _ mcp' _).1 => /=.
        by move: Ht'; rewrite Ht => -[] <-.
Qed.

Fixpoint kmtree_flatten tree :=
  if tree is Br mc f
  then mc :: flatten [seq kmtree_flatten (f t) | t : transition]
  else [::].

Lemma kmtree_flattenP tree : kmtree_flatten tree =i tree.
Proof.
  elim: tree=> // m f IHtree mc; rewrite !inE -/kmtree_flatten; congr (_ || _).
  apply/flatten_imageP/existsP=> [[t _ ?]|[t ?]]; exists t => //.
  - by rewrite -IHtree.
  - by rewrite IHtree.
Qed.

Definition kmtree_has P tree := has P (kmtree_flatten tree).

Lemma kmtree_hasP (P : pred markingc) (tree : kmtree) : 
  reflect (exists mc, mc \in tree /\ P mc) 
          (kmtree_has P tree).
Proof.
  apply: (iffP hasP).
  - by case=> mc mcintree Pmc; exists mc; rewrite -kmtree_flattenP.
  - by case=> mc [mcintree Pmc]; exists mc; rewrite ?kmtree_flattenP.
Qed.

Definition kmtree_coverable tree m := kmtree_has (fun mc => m \in mc) tree.

Definition mkkmt_init (m : marking) : {mcp | mkkmt_inv mcp.1 mcp.2} :=
  Sub ([::], embedm m) isT. 

Lemma in_mkkmtree_initP m0 mc:
  reflect (exists s mcs,
              ([::], embedm m0) ={! s !}> (mcs, mc) /\ uniq (rcons mcs mc))
          (mc \in mkkmtree (mkkmt_init m0)).
Proof. by move: (in_mkkmtreeP (mkkmt_init m0) mc); rewrite andbT. Qed.

Lemma mkkmtree_coverable_sound (m0 m : marking) :
  kmtree_coverable (mkkmtree (mkkmt_init m0)) m ->
  exists s m', m0 ={ s }> m' /\ m <= m'.
Proof.
  case/kmtree_hasP=> mc [/in_mkkmtree_initP [s [mcs [Hs _]]] Hin].
  exact: (nextma_seq_cover_sound Hs).
Qed. 

Lemma mkkmtree_coverable_complete (m0 m : marking) s :
  m0 ={ s }> m -> kmtree_coverable (mkkmtree (mkkmt_init m0)) m.
Proof.
  case/nextma_seq_cover_complete=> mc [Hin] /in_mkkmtree_initP H.
  by apply/kmtree_hasP; exists mc.
Qed.

(* Main theorem: the Karp-Miller tree for a Petri net provides us with
   a decision procedure for coverability.  *)

Theorem mkkmtree_coverableP m0 m :
  reflect (coverable m0 m) (kmtree_coverable (mkkmtree (mkkmt_init m0)) m).
Proof.
  apply: (iffP idP).
  - move /mkkmtree_coverable_sound => [s [m' [H1 H2]]].
    exists m'.
    split => //.
    by exists s.
  - case => m' [[s]].
    move /mkkmtree_coverable_complete.
    move /kmtree_hasP => [mc [H1 H2]] H.
    apply /kmtree_hasP.
    exists mc.
    split => //.
    by apply: mem_le_mc H.
Qed.


(* We have proved the completeness via in_mkkmtreeP, which relates the
   membership in a Karp-Miller tree and the reachability in the
   transition system with the acceleration (_ ={! s !}> _).  Actually,
   it is possible to prove the completeness directly.  But it makes
   both the statement and the proof really complicated as below.  *)

Section AlternativeCompletenessProof.

Section Prefix.

Variable T : eqType.

Definition prefix (s1 s2 : seq T) := take (size s1) s2 == s1.

Lemma prefix_refl s : prefix s s.
Proof. by rewrite /prefix take_size. Qed.

Lemma prefixP s1 s2 : reflect (exists s, s1 ++ s = s2) (prefix s1 s2).
Proof.
  apply: (iffP eqP) => [<- | [s <-]]; last by rewrite take_size_cat.
  exists (drop (size s1) s2); by rewrite cat_take_drop.
Qed.

End Prefix.

Definition path_prefix mcs (mc : markingc) :=
  {mcp : seq markingc * markingc | prefix (rcons mcp.1 mcp.2) (rcons mcs mc)}.

Lemma mkkmtree_cover_complete_nextmc_seq mc mc1 s :
  mc ={` s `}> mc1 ->
  forall mcs (Hinv : forall smcp : path_prefix mcs mc,
                            mkkmt_inv (val smcp).1 (val smcp).2),
    uniq (rcons mcs mc) ->
    exists mc1' (smcp : path_prefix mcs mc),
      mc1' \in mkkmtree (Sub (val smcp) (Hinv smcp)) /\ mc1 <= mc1'.
Proof.
  elim: s => [ | t s IHs] in mc mc1 *.
  - rewrite /=.
    case => -> mcs H Huniq.
    exists mc1.
    apply: ex_intro (Sub (mcs, mc1) _) _; first by rewrite prefix_refl.
    rewrite /= => Hyp.
    split => //.
    by rewrite mkkmtreeE /= inE eqxx.
  - rewrite foldm_cons_some.
    case => m' [H1 H2] mcs Hinv Huniq.
    case E: (nextma (mcs, mc) t) => [[mcs' mc']|]; last by case: nextmaP E H1 => //= ->.
    case: nextmaP H1 (E) => // _ -> [->] [] Hmcs' Hmc'.
    case: (@nextmc_seq_ucompat _ _ _ mc' _ H2); first by rewrite -Hmc'; apply: lemc_accel.
    move => mcx [H3 H4].
    pose i := index mc' (rcons mcs' mc').
    case: (IHs _ _ H3 (take i (rcons mcs' mc'))).
    + move => [/= mcp /prefixP [/= mcs2]].
      case: (lastP mcs2) => [ | mcs2b mcs2l] /eqP; last first.
      * rewrite -rcons_cat eqseq_rcons.
        move /andP => [] Heq _.
        apply: (Hinv (Sub mcp _)).
        apply /prefixP.
        exists (mcs2b++drop i mcs').
        rewrite catA.
        move: Heq.
        move /eqP => ->.
        rewrite Hmcs' -cats1 takel_cat; first by rewrite cat_take_drop.
        by rewrite -ltnS -(size_rcons _ mc') index_mem mem_rcons inE eqxx.
      * rewrite cats0 eqseq_rcons.
        move /andP => [] /eqP => -> /eqP => ->.
        rewrite /i -{1}cats1 index_cat.
        case: ifP => [Hin | Hnotin].
        * apply: (Hinv (Sub ((take (index mc' mcs') (rcons mcs' mc')), mc') _)) => /=.
          apply /prefixP.
          exists  (drop (index mc' mcs').+1 mcs').
          rewrite Hmcs' -[rcons mcs' mc']cats1 takel_cat; last by apply: index_size.
          rewrite cat_rcons.
          rewrite -{2}(nth_index mc' Hin) -drop_nth;last by rewrite index_mem.
          by rewrite cat_take_drop.
        * rewrite /= eqxx addn0 -cats1 takel_cat // take_size.
          apply: mkkmt_inv_nextma (Sub (mcs, mc) _) _ _ E.
          apply: Hinv (Sub (mcs, mc) _).
          by rewrite prefix_refl.
    + rewrite /i -{1}[rcons mcs' mc']cats1 index_cat.
      case: ifP => [Hin | Hnotin].
      * rewrite -[rcons mcs' _]cats1 takel_cat; last by apply: index_size.
        rewrite -{2}(nth_index mc' Hin) -take_nth;last by rewrite index_mem.
        move: Huniq.
        rewrite Hmcs' -{1}(cat_take_drop (index mc' mcs').+1 mcs').
        rewrite cat_uniq.
        by case: uniq.
      * rewrite /= eqxx addn0 -[rcons mcs' _]cats1 take_size_cat => //.
        rewrite rcons_uniq.
        apply /andP; split; first by apply /negPf.
        by rewrite -Hmcs'.
    + move => Hinv' mcx' [smcp] [Htree H5].      
      exists mcx'.
        have [ Hin | Hnotin ] := boolP (mc' \in mcs').
        {- apply: ex_intro _ (Sub (val smcp) _) _.
           + case : (smcp) => [[smcp1 smcp2]] /=.
             case /prefixP => mcs2 Heq.
             rewrite Hmcs'.
             apply /prefixP.
             exists (mcs2 ++ drop i.+1 mcs').
             rewrite catA Heq.
             have Hin' : mc' \in rcons mcs' mc' by rewrite mem_rcons inE eqxx.
             rewrite -{2}(nth_index mc' Hin') -take_nth; last by rewrite index_mem.
             rewrite -cats1 takel_cat; first by rewrite cat_take_drop.
             by rewrite /i -cats1 index_cat Hin index_mem.
           + move => Hyp.
             split; last by apply: le_trans H5.
             rewrite [Sub _ _]( _ : _ = (Sub (val smcp) (Hinv' smcp))); first by apply: Htree.
             by apply: val_inj. }
        {- case /prefixP : (svalP smcp) => mcs2.
           rewrite {3}(_ : i = size mcs'); last by rewrite /i -cats1 index_cat (negPf Hnotin) index_head addn0.
           rewrite -{3}[rcons mcs' mc']cats1 takel_cat // take_size.
           case: (lastP mcs2) => [ | mcs2b mcs2l] /eqP; last first.
           * rewrite -rcons_cat eqseq_rcons. 
             move /andP => [] Heq _.
             apply: ex_intro _ (Sub (val smcp) _) _.
             apply /prefixP.
             exists mcs2b.
             by rewrite (eqP Heq) Hmcs'.
             move => H6.
             split; last by apply: le_trans H5.
             rewrite [Sub _ _]( _ : _ = (Sub (val smcp) (Hinv' smcp))) //.
             by apply: val_inj.
           * rewrite cats0 eqseq_rcons.
             move /andP => [] /eqP Heq1 /eqP Heq2.
             apply: ex_intro _ (Sub (mcs, mc) _) _ ; first by rewrite prefix_refl.
             move => Heq.
             split; last by apply: le_trans H5.        
             rewrite mkkmtreeE /=.
             apply /predU1P.
             right.
             apply /existsP.
             exists t.
             rewrite ffunE.
             case: dec_op; last by move => H6; rewrite H6 in E.
             move => [[mcs'' mc''] E'] /=.
             move: E.
             rewrite E'.
             case => H6 H7.
             rewrite -H6 -H7 in Hnotin.
             case: ifP; first by rewrite (negPf Hnotin).
             move => Hnotin'.
             rewrite [Sub _ _]( _ : _ = (Sub (val smcp) (Hinv' smcp))); first by apply: Htree.
             apply: val_inj.
             by rewrite [RHS]surjective_pairing Heq1 Heq2 -H6 -H7. }
Qed.

Lemma nextmc_embedl (m1 : marking) mc2 t:
  embedm m1 =1{` t `}> mc2 -> exists m2, mc2 = embedm m2 /\ m1 =1{ t }> m2.
Proof.
  case: nextmcP => //=; rewrite inE -embedm_sub -embedm_add /nextm => -> [<-].
  by exists (m1 :-: pre t :+: post t).
Qed.

Lemma nextm_seq_embed (m1 m2 : marking) s:
  m1 ={ s }> m2 <-> embedm m1 ={` s `}> embedm m2.
Proof.
  split.
  - elim: s => [| t s IHs] in m1 *; first by case=> ->.
    rewrite !foldm_cons_some => -[m3 [Hm3t /IHs Hem3s]].
    by exists (embedm m3); split=> //; apply/nextm_embed.
  - elim: s => [| t s IHs] in m1 *; first by case=> /embedm_inj <-.
    rewrite !foldm_cons_some => -[mc3 [/nextmc_embedl [m3 [-> Hm1t]] /IHs Hm3s]].
    by exists m3.
Qed.

Lemma mkkmtree_coverable_complete' (m0 m : marking) s :
  m0 ={ s }> m -> kmtree_coverable (mkkmtree (mkkmt_init m0)) m.
Proof.
  move/nextm_seq_embed => Hm0s.
  apply/kmtree_hasP.
  have Hprefix1 mc :
    forall pmcs : path_prefix [::] mc, val pmcs = ([::], mc).
  { move=> [[mcs1 mc1] /= /prefixP /= [mcs2]].
    rewrite -[[:: mc]]/(rcons [::] mc) => /eqP.
    case/lastP: mcs2 => [|mcs2 mc2].
    - by rewrite cats0 eqseq_rcons => /andP [/eqP -> /eqP ->].
    - by rewrite -rcons_cat eqseq_rcons cat_rcons => /eqP; case: mcs1. }
  case: (@mkkmtree_cover_complete_nextmc_seq _ _ _ Hm0s [::]) => //.
  - by move=> smcp; rewrite (Hprefix1 _ smcp).
  - move=> Hyp mc [pmcs [H1 H2]]; exists mc; rewrite mem_mcP; split=> //.
    rewrite /mkkmt_init [Sub _ _](_ : _ = Sub (val pmcs) (Hyp pmcs)) //.
    by apply: val_inj; rewrite /= (Hprefix1 _ pmcs).
Qed.

End AlternativeCompletenessProof.

(* Boundedness *)

Definition kmtree_maximum (p : place) (tree : kmtree) : natc :=
  \join_(mc <- kmtree_flatten tree) mc p.

Lemma kmtree_maximum_coverable (p : place) (tree : kmtree) mc :
  mc \in tree -> mc p <= kmtree_maximum p tree.
Proof.
  rewrite -kmtree_flattenP /kmtree_maximum.
  elim: (kmtree_flatten tree) => [//|shd stl IHs].
  rewrite inE big_cons.
  move/predU1P=> [->|mc_stl].
    by rewrite leUl.
  by rewrite lexU // IHs // orbT.
Qed.

Lemma kmtree_has_maximum (p : place) (tree : kmtree) :
  (exists mc, mc \in tree) ->
  kmtree_has (fun mc : markingc => mc p == (kmtree_maximum p tree)) tree.
Proof.
  case=> mc.
  rewrite -kmtree_flattenP => H.
  apply/hasP.
  have i0 : seq_sub (kmtree_flatten tree) := Sub mc H.
  exists (val [arg max_(i > i0) (val i) p : natc]); first by apply:valP.
  rewrite /kmtree_maximum big_tnth.
  case: arg_maxP => //= i _ H2.
  rewrite (@eq_le _ natc). apply/andP. split.
  have/seq_tnthP [ii ->]: val i \in kmtree_flatten tree by apply:valP.
    by apply: (@joins_sup _ _ _ ii).
  apply/joinsP=> j_index _.
  set j := tnth _ _.
  have j_in: j \in kmtree_flatten tree by apply: mem_tnth.
  by apply: (H2 (Sub j j_in)).
(*  case=> mc.
  rewrite -kmtree_flattenP /kmtree_has /kmtree_maximum.
  elim: (kmtree_flatten tree) => [//|shd [|stlhd stltl] IHs] in mc *.
    by rewrite /= big_seq1 eqxx.
  move=> _.
  move/(_ stlhd): IHs.
  rewrite inE eqxx [_ :: _]lock big_cons /=.
  set m := \join_(_ <- _|_) _.
  move/(_ isT)=> IHs.
  case/orP: (le_total m (shd p));
    first by move/join_idPr=> ->; rewrite eqxx.
  by move/join_idPl=> ->; rewrite IHs orbT. *)
Qed.

Theorem mkkmtree_boundedP m0 p :
  reflect {for p, bounded m0}
          (~~ kmtree_has (fun mc : markingc => mc p == \top :> natc)
              (mkkmtree (mkkmt_init m0))).
Proof.
  apply: (iffP negP).
  - pose nc := kmtree_maximum p (mkkmtree (mkkmt_init m0)).
    case nc_kind: nc => [n|].
    - move=> _; exists n => m [s /mkkmtree_coverable_complete].
      case/kmtree_hasP=> mc [mc_in_tree /forallP /(_ p)] mp_in_mcp.
      move/kmtree_maximum_coverable: mc_in_tree => /(_ p).
      by rewrite -/nc nc_kind => /lencP; apply.
    - case; rewrite -[\top]nc_kind; apply: kmtree_has_maximum.
      by exists (embedm m0); rewrite mkkmtreeE inE eqxx.  
  - case=> n bounded_p /kmtree_hasP [mc [mc_in_tree /eqP mc_p_top]].
    pose m : marking := [ffun p' => if p' == p then n.+1 else 0].
    have: kmtree_coverable (mkkmtree (mkkmt_init m0)) m.
      apply/kmtree_hasP; exists mc; split=> //; apply/forallP=> p'.
      rewrite ffunE; case: eqP => [->|]; last by rewrite inE.
      by rewrite mc_p_top.
    case/mkkmtree_coverable_sound=> s [m' [reach_m0_m']] /forallP /(_ p).
    apply/negP;  rewrite leEnat -ltnNge ffunE eqxx ltnS -leEnat bounded_p //.
    by exists s.
Qed.

Arguments mkkmtree_boundedP {m0 p}.

Theorem mkkmtree_multi_place_boundedP m0 (ps : {set place}):
  reflect {in ps, bounded m0} [forall p in ps, ~~ kmtree_has (fun mc : markingc => mc p == \top :> natc) (mkkmtree (mkkmt_init m0))].
Proof.
  apply: (iffP forall_inP).
  - move=> ps_finite p p_in_ps.
    apply/mkkmtree_boundedP.
    exact: ps_finite.
  - move=> ps_bounded p p_in_ps.
    apply/mkkmtree_boundedP.
    exact: ps_bounded.
Qed.

Definition kmtree_has_no_top tree : bool :=
  ~~ kmtree_has [preim @topsmc pn of predC1 set0] tree.

Theorem mkkmtree_all_place_boundedP m0 :
  reflect (bounded m0) (kmtree_has_no_top (mkkmtree (mkkmt_init m0))).
Proof.
  apply: (equivP idP); rewrite /bounded (rwP 'forall_mkkmtree_boundedP).
  rewrite -negb_exists /kmtree_has (sameP existsP fin_exists_hasP).
  rewrite /kmtree_has_no_top /kmtree_has.
  set a1 := [preim _ of _]; set a2 := fun _ => _.
  rewrite (eq_has (_ : a1 =1 a2)) // {}/a1 {}/a2 /topsmc => mc /=.
  rewrite inE (sameP (set0Pn _) existsP); apply: eq_existsb => p.
  by rewrite inE.
Qed.

Lemma embedm_in_kmtree_reachable m0 m :
  embedm m \in mkkmtree (mkkmt_init m0) -> reachable m0 m.
Proof.
  case/in_mkkmtree_initP=> s [mcs [m0_accs_m _]].
  exists s.
  apply/nextm_seq_embed.
  apply: (nextma_no_acc_nextmc_seq m0_accs_m).
  by rewrite !topsmc_embed.
Qed.

Theorem bounded_reachableP m0 :
  bounded m0 -> forall m, reflect (reachable m0 m) (embedm m \in mkkmtree (mkkmt_init m0)).
Proof.
  move=> bounded_m0 m.
  apply: (iffP idP); first by apply: embedm_in_kmtree_reachable.
  case=> _ /nextma_seq_proj_cover_complete [ps /in_mkkmtree_initP].
  have [ps_C0|ps_Cn0] := altP (~: ps =P set0).
    by rewrite -[ps]setCK ps_C0 setC0 projmcT.
  move=> proj_m_in_tree.
  move/mkkmtree_all_place_boundedP: bounded_m0.
  case/negP. apply/kmtree_hasP.    
  exists (projmc ps (embedm m)). split => //=.
  by rewrite topsmc_proj_embed inE.
Qed.

(* Termination *)

Fixpoint kmtree_has_Lf tree :=
  match tree with
  | DLf => false
  | Lf _ => true
  | Br _ f => [exists t, kmtree_has_Lf (f t)]
  end.

Theorem mkkmtree_terminateP m0 :
  reflect (terminate m0)
          (predD kmtree_has_no_top kmtree_has_Lf (mkkmtree (mkkmt_init m0))).
Proof.
  apply: (iffP andP) => [[]|].
  - suff/(_ (mkkmtree (mkkmt_init m0)) (mkkmt_init m0) m0):
      forall tree mcp m, tree = mkkmtree mcp -> (val mcp).2 = embedm m ->
                         ~~ kmtree_has_Lf tree -> kmtree_has_no_top tree ->
                         terminate m
        by move/(_ (erefl _)) => /= /(_ (erefl _)).
    elim=> [mcp m|mcp m|mc subtrees IHtree mcp m]; [by rewrite mkkmtreeE..|].
    rewrite mkkmtreeE => -[_ subtrees_ffun mcp2_m].
    rewrite /= negb_exists => /forallP neg_has_Lf /negP has_no_top.
    constructor=> m1 [t m_t_m1].
    move/ffunP/(_ t): subtrees_ffun; rewrite ffunE.
    case: dec_op => [[mcp1 mcp_t_mcp1]|]; last first.
      by case: nextmaP => [//|]; move/nextm_embed: m_t_m1; rewrite mcp2_m => ->.
    case: ifP => /= [_ sub_t_Lf_mcp12|mcp12_notin_mcp11 subtrees_t_eq].
      by case/negP: (neg_has_Lf t); rewrite sub_t_Lf_mcp12.
    case: nextmaP (mcp_t_mcp1) => [mc1|//].
    rewrite mcp2_m => /embedm_nextmc_nextm [m1' [{mc1}->]].
    rewrite m_t_m1 => -[{m1'}<-] [] eq_mcp1.
    apply: (IHtree _ _ _ subtrees_t_eq) => //=; last first.
      apply/negP=> /kmtree_hasP [mc' [mc'_in_tree mc'_p_1]].
      apply: has_no_top. apply/kmtree_hasP. exists mc'. split=> //.
      rewrite inE. apply/orP. right. apply/existsP. by exists t.
    rewrite -eq_mcp1 => /=.
    apply/ffunP=> p. case: accelmcP => [_ _ <- //|_ mc' H1 H2 H3 _|//].
    case: has_no_top.
    apply/kmtree_hasP. exists mcp1.2.
    split.
      rewrite inE. apply/orP. right. apply/existsP. exists t.
      by rewrite subtrees_t_eq mkkmtreeE inE /= eqxx orTb.
    rewrite -eq_mcp1 /= inE /topsmc. apply/set0Pn. exists p. rewrite inE.
    by case: accelmcP => [n' /(_ mc' H1 H2) /negP []|//|//].
  - move=> terminate_m0.
    suff /(_ (mkkmt_init m0)) :
      forall (mcp : {mcp : seq markingc * markingc | mkkmt_inv mcp.1 mcp.2}),
        (val mcp).2 = embedm m0 ->
        (forall (mc1 : markingc) m2 s, mc1 \in (val mcp).1 -> m0 ={ s }> m2 ->
                                       ~ mc1 <= embedm m2) ->
        ~~ kmtree_has_Lf (mkkmtree mcp) /\ kmtree_has_no_top (mkkmtree mcp)
        by apply.
    elim/terminate_ind: m0 / terminate_m0 => m0 IH IH2 mcp mcp2_m0 mc1_nle_m2.
    have terminate_m0 : terminate m0 by constructor=> y [? /IH].
    have IH2_Hs t mcp1 :
      (val mcp) =1{! t !}> mcp1 ->
      exists m1, m0 =1{ t }> m1 /\ mcp1.2 = embedm m1 /\
                 forall (mc1 : markingc) m2 s,
                   mc1 \in mcp1.1 -> m1 ={ s }> m2 -> ~ mc1 <= embedm m2.
    { case: nextmaP => // mc1.
      rewrite mcp2_m0 => /embedm_nextmc_nextm [m1 [{mc1}-> m0_t_m1]] [{mcp1}<-].
      exists m1; repeat split => //=.
      - apply/ffunP=> p; case: accelmcP => n // mc'.
        rewrite mem_rcons inE => /predU1P [{mc'}->|mc'_in_mcp1 ?].
          rewrite lemc_embed => ?.
          case/terminate_no_lasso/(_ [::] m0 t [::] m1): terminate_m0 => //.
          by rewrite foldm1.
        by case/(mc1_nle_m2 _ m1 [:: t]): mc'_in_mcp1 => //; rewrite foldm1.
      - move=> mc1 m2 s; rewrite mem_rcons inE => /predU1P [-> m1_s_m2|? ?].
          rewrite lemc_embed.
          move/terminate_no_lasso/(_ [::] _ t s): terminate_m0.
          by apply=> //; rewrite foldm_cons_some; exists m1.
        apply: (mc1_nle_m2 _ _ (t :: s)) => //.
        by rewrite foldm_cons_some; exists m1. }
    split.
    + rewrite mkkmtreeE.
      set subtrees := [ffun t => _]; rewrite /=; apply/negP=> /existsP [t].
      rewrite ffunE; case: dec_op => [[mcp1 mcp_t_mcp1]|//].
      rewrite ifN; last first.
        case: nextmaP (mcp_t_mcp1) => // mc1.
        rewrite mcp2_m0 => /embedm_nextmc_nextm [m1 [{mc1}-> m0_t_m1]] [<-].
        rewrite /= mem_rcons inE.
        apply/negP=> /predU1P [mcp12_m0|].
          case/terminate_no_lasso/(_ [::] m0 t [::] m1): terminate_m0 => //.
            by rewrite foldm1.
          suff /(_ _ _ _ mcp12_m0) -> : forall (mcs : seq markingc) m m',
            accelmc mcs (embedm m) = embedm m' -> m = m' by [].
          move=> mcs m m' /ffunP H; apply/ffunP=> p; move: (H p).
          by rewrite !ffunE; case: ifP => // _ [].
        move/mc1_nle_m2=> /(_ m1 [:: t]); case; first by rewrite foldm1.
        apply/forallP=> p; case: accelmcP => [_ _ <- //|n mc'0|-> //].
        rewrite mem_rcons inE => /predU1P [->|].
          rewrite lemc_embed => ?.
          case/terminate_no_lasso/(_ [::] m0 t [::] m1): terminate_m0 => //.
          by rewrite foldm1.
        by move/(mc1_nle_m2 mc'0 m1 [:: t]); rewrite foldm1 => /(_ m0_t_m1).
      case/IH2_Hs: (mcp_t_mcp1) => m1 [m0_t_m1 [H1 H2]].
      move: (IH2 _ _ m0_t_m1 (Sub mcp1 (mkkmt_inv_nextma mcp_t_mcp1)) H1 H2).1.
      by move/negP.
    + apply/negP=> /kmtree_hasP [mc [mc_in_tree /= mc_has_top]].
      move: mc_in_tree; rewrite mkkmtreeE inE.
      move/predU1P=> [mc_mcp2|/existsP [t]].
        by move: mc_has_top; rewrite mc_mcp2 mcp2_m0 topsmc_embed inE eqxx.
      rewrite ffunE; case: dec_op => [[mcp1 mcp_t_mcp1]|//].
      case: ifP => //= mcp12_notin_mcp11 mc_in_tree.
      case/IH2_Hs: (mcp_t_mcp1) => m1 [m0_t_m1 [H1 H2]].
      move: (IH2 _ _ m0_t_m1 (Sub mcp1 (mkkmt_inv_nextma mcp_t_mcp1)) H1 H2).2.
      by case/negP; apply/kmtree_hasP; exists mc.
Qed.

End KarpMillerTree.
