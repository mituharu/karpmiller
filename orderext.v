(* Some extensions to order.v.  *)

From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
Import Order.TTheory.
Require Import ssreflectext.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.


Section ComponetwiseRelation.

Context (aT : finType).
Context {disp : Order.disp_t}.

Section POrder.

Context (T : porderType disp).

HB.instance Definition _ :=
  Order.isPOrder.Build disp {ffun aT -> T} (rrefl _)
    (relcw_refl (aT:=aT) (le_refl (T:=T)))
    (relcw_anti le_anti) (relcw_trans le_trans).

End POrder.

Section Lattice.

Context (L : latticeType disp).

Lemma lecwEmeet (x y : {ffun aT -> L}) : (x <= y) = ((fflift2 Order.meet) x y == x).
Proof.
  apply/forallP/eqP.
  - by move=> H; apply/ffunP => a; rewrite !ffunE; apply/eqP; rewrite -leEmeet.
  - by move=> H a; move/ffunP/(_ a): H; rewrite !ffunE => /eqP; rewrite leEmeet.
Qed.

HB.instance Definition _ :=
  Order.POrder_isLattice.Build _ {ffun aT -> L}
    (fflift2_commutative (meetC (L:=L)))
    (fflift2_commutative (joinC (L:=L)))
    (fflift2_associative (meetA (L:=L)))
    (fflift2_associative (joinA (L:=L)))
    (fflift2_left_absorptive (joinKI (L:=L)))
    (fflift2_left_absorptive (meetKU (L:=L)))
    lecwEmeet.

End Lattice.

Section BLattice.

Context (L : bLatticeType disp).

Lemma lecw0x (x : {ffun aT -> L}) : [ffun=> \bot] <= x.
Proof. by apply/forallP => a; rewrite ffunE le0x. Qed.

HB.instance Definition _ := Order.hasBottom.Build _ {ffun aT -> L} lecw0x.

End BLattice.

Section TBLattice.

Context (L : tbLatticeType disp).

Lemma lecwx1 (x : {ffun aT -> L}) : x <= [ffun=> \top].
Proof. by apply/forallP => a; rewrite ffunE lex1. Qed.

HB.instance Definition _ := Order.hasTop.Build _ {ffun aT -> L} lecwx1.

End TBLattice.

End ComponetwiseRelation.

(***************************************)
(* We declare a "copy" of the options, *)
(* with None as the top element.       *)
(***************************************)

Module OptionTopOrder.
Section OptionTopOrder.

Fact top_display (disp : Order.disp_t) : Order.disp_t. Proof. exact: disp. Qed.

Definition type (disp : Order.disp_t) T := option T.
Definition type_ (disp : Order.disp_t) (T : porderType disp) :=
  type (top_display disp) T.

Context {disp disp' : Order.disp_t}.

Local Notation option := (type disp').

#[export] HB.instance Definition _ (T : eqType) := Equality.on (option T).
#[export] HB.instance Definition _ (T : choiceType) := Choice.on (option T).
#[export] HB.instance Definition _ (T : countType) := Countable.on (option T).
#[export] HB.instance Definition _ (T : finType) := Finite.on (option T).

Section POrder.
Context (T : porderType disp).
Implicit Types (ox : option T).

Definition le ox1 ox2 := if ox2 is Some x2
                         then if ox1 is Some x1 then x1 <= x2 else false
                         else true.

Definition lt ox1 ox2 := if ox1 is Some x1
                         then if ox2 is Some x2 then x1 < x2 else true
                         else false.

Fact lt_def x y : lt x y = (y != x) && (le x y).
Proof. by case: x y => [?|] [?|] //=; rewrite lt_def. Qed.

Fact refl : reflexive le.
Proof. by move=> [?|] /=. Qed.

Fact anti : antisymmetric le.
Proof. by move=> [?|] [?|] //= /le_anti ->. Qed.

Fact trans : transitive le.
Proof. by move=> [?|] [?|] [?|] //=; apply: le_trans. Qed.

#[export]
HB.instance Definition _ :=
  Order.isPOrder.Build disp' (option T) lt_def refl anti trans.

Fact lex1 x : x <= None :> option T.
Proof. by []. Qed.

#[export]
HB.instance Definition _ := Order.hasTop.Build _ (option T) lex1.

Lemma leEoptiontop ox1 ox2 :
  ox1 <= ox2 = if ox2 is Some x2
               then if ox1 is Some x1 then x1 <= x2 else false
               else true.
Proof. by []. Qed.

Lemma ltEoptiontop ox1 ox2 :
  ox1 < ox2 = if ox1 is Some x1
              then if ox2 is Some x2 then x1 < x2 else true
              else false.
Proof. by []. Qed.

Lemma topEoptiontop : \top = None :> option T.
Proof. by []. Qed.

End POrder.

Section BPOrder.
Context (T : bPOrderType disp).

Fact le0x x : Some \bot <= x :> option T.
Proof. by case: x => [?|] //; rewrite leEoptiontop. Qed.

#[export]
HB.instance Definition _ := Order.hasBottom.Build _ (option T) le0x.

Lemma botEoptiontop : \bot = Some \bot :> option T.
Proof. by []. Qed.

End BPOrder.

Section Total.

Context (T : orderType disp).

Fact total : total (<=%O : rel (option T)).
Proof. by move=> [?|] [?|] //=; apply: le_total. Qed.

#[export]
HB.instance Definition _ := Order.POrder_isTotal.Build _ (option T) total.

End Total.

(* FIXME: use HB.saturate *)
#[export]
HB.instance Definition _ (T : bOrderType disp) := Order.POrder.on (option T).
(* /FIXME *)

End OptionTopOrder.

Module Exports.

HB.reexport OptionTopOrder.

Notation optiontop_with := type.
Notation optiontop := type_.

Definition leEoptiontop := @leEoptiontop.
Definition ltEoptiontop := @ltEoptiontop.
Definition topEoptiontop := @topEoptiontop.
Definition botEoptiontop := @botEoptiontop.

End Exports.

End OptionTopOrder.
HB.export OptionTopOrder.Exports.
