(* Karp-Miller acceleration on Petri nets.
   By Mitsuharu Yamamoto (overall design and refactoring),
      Shogo Sekine (first implementation of soundness), and
      Saki Matsumoto (completeness).  *)

From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
Import Order.TTheory.
Require Import ssreflectext orderext monad petrinet.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section MarkingDef.

Variable pn : petri_net.

Notation place := (place pn).
Notation marking := (marking pn).

(* Representation of the completion/ideals of (nat, <=).  Each ideal
   is represented by an element of option nat (named "natc");
     * "Some n" for the downward closure of n (i.e., {n' | n' <=  n})
       (denoted by "embedn n" later)
     * "None" for the set of natural numbers ("omega" in the literature).
       (denoted by "top" later)
   So the membership between nat and natc can be defined in a natural
   way.  *)
Definition natc := optiontop nat.
Definition mem_nc (nc : natc) :=
  if nc is Some n then fun n' => (n' <= n)%N else xpredT.
Canonical natc_predType := PredType mem_nc.

Definition embedn (n : nat) : natc := Some n.

(* Addition/subtraction of a natural number to/from a natc value.  *)
Definition addncn (nc : natc) (n : nat) : natc := omap (addn^~ n) nc.
(*  if nc is Some n' then Some (n' + n) else None.  *)
Definition subncn (nc : natc) (n : nat) : natc := omap (subn^~ n) nc.
(*  if nc is Some n' then Some (n' - n) else None.  *)

(* Representation of the completion/ideals of (marking, <=).  The
   construction from nat and natc is lifted to finfun: marking to
   markingc.  Membership is attached likewise.  *)
Definition markingc := {ffun place -> natc}.
Definition mem_mc (mc : markingc) (m : marking) := [forall p, m p \in mc p].
Canonical markingc_predType := PredType mem_mc.

Definition embedm : marking -> markingc := fflift embedn.

(* Addition/subtraction of a marking to/from a markingc value.  *)
Definition addmcm : markingc -> marking -> markingc := fflift2 addncn.
Definition submcm : markingc -> marking -> markingc := fflift2 subncn.

(* Projection of the markingc value "mc": assign the top element
   (omega) to the places not in the set "ps".  *)
Definition projmc (ps : {set place}) (mc : markingc) : markingc :=
  [ffun p => if p \in ps then mc p else \top].
(* fflift2 (fun b nc => if b then nc else top) (val ps) mc.*)

(* The set of places where omega is assigned.  *)
Definition topsmc (mc : markingc) : {set place} :=
  [set p | mc p == \top :> natc].
(* innew (fflift (pred1 top) mc).*)

End MarkingDef.

Prenex Implicits addmcm submcm embedm.

Notation "nc `+ n" := (addncn nc n) (at level 50, left associativity)
                      : petri_net_scope.
Notation "nc `- n" := (subncn nc n) (at level 50, left associativity)
                        : petri_net_scope.
Notation "mc :`+: m" := (addmcm mc m) (at level 50, left associativity)
                        : petri_net_scope.
Notation "mc :`-: m" := (submcm mc m) (at level 50, left associativity)
                        : petri_net_scope.

Section TransitionDef.

Variable pn : petri_net.

Notation marking := (marking pn).
Notation markingc := (markingc pn).

(* One-step transition on completed markings.  *)
Definition nextmc (mc : markingc) t : option markingc :=
  if pre t \in mc then Some (mc :`-: pre t :`+: post t) else None.

(* Karp-Miller acceleration operation.  We need the history of past
   (i.e., ancestors in the Karp-Miller tree) completed markings "mcs".  *)
Definition accelmc (mcs : seq markingc) (mc : markingc) : markingc :=
  projmc [set p | ~~ has (fun mc' => (mc' <= mc) && (mc' p < mc p)) mcs] mc.

(* One-step transition with Karp-Miller acceleration.  The sequence
   "mcp.1" represents the accumulated history of completed markings.  *)
Definition nextma (mcp : seq markingc * markingc) t :=
  omap (fun mc => (rcons mcp.1 mcp.2, accelmc (rcons mcp.1 mcp.2) mc))
       (nextmc mcp.2 t).
(*
  if nextmc mcp.2 t is Some mc
  then Some (rcons mcp.1 mcp.2, accelmc (rcons mcp.1 mcp.2) mc)
  else None.
*)

End TransitionDef.

Prenex Implicits nextmc nextma.

(* One-step transition on completed markings.  *)
Notation "mc1 =1{` t `}> mc2" :=
  (nextmc mc1 t = Some mc2)
    (at level 70, t at next level,
     format "'[hv ' mc1 '/' '['  =1{`  t  `}>  ']' mc2 ']'")
  : petri_net_scope.
(* Multi-step transition on completed markings.  *)
Notation "mc1 ={` s `}> mc2" :=
  (foldm nextmc mc1 s = Some mc2)
    (at level 70, s at next level,
     format "'[hv ' mc1 '/' '['  ={`  s  `}>  ']' mc2 ']'")
  : petri_net_scope.
(* One-step transition with Karp-Miller acceleration.  *)
Notation "mcp1 =1{! t !}> mcp2" :=
  (nextma mcp1 t = Some mcp2)
    (at level 70, t at next level,
     format "'[hv ' mcp1 '/' '['  =1{!  t  !}>  ']' mcp2 ']'")
  : petri_net_scope.
(* Multi-step transition with Karp-Miller accelerations.  *)
Notation "mcp1 ={! s !}> mcp2" :=
  (foldm nextma mcp1 s = Some mcp2)
    (at level 70, s at next level,
     format "'[hv ' mcp1 '/' '['  ={!  s  !}>  ']' mcp2 ']'")
  : petri_net_scope.

Section Theory.

Variable pn : petri_net.

Notation place := (place pn).
Notation transition := (transition pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).

Lemma mem_ncP n (nc : natc) : (n \in nc) = (embedn n <= nc).
Proof. by case: nc. Qed.

Lemma in_top n : n \in (\top : natc).
Proof. by []. Qed.

Lemma in_embedn n1 n2 : (n1 \in embedn n2) = (n1 <= n2)%N.
Proof. by []. Qed.

Lemma in_0_nc (nc : natc) : 0 \in nc.
Proof. by rewrite mem_ncP le0x. Qed.

Lemma lenc_embed : {mono embedn : n1 n2 / n1 <= n2}.
Proof. by []. Qed.

Lemma ltnc_embed : {mono embedn : n1 n2 / n1 < n2}.
Proof. by []. Qed.

(* Any natc value is downward closed. *)
Lemma mem_le_nc n1 n2 (nc : natc) : n1 \in nc -> n2 <= n1 -> n2 \in nc.
Proof.
  by rewrite !mem_ncP => Hin Hle; rewrite (le_trans _ Hin).
Qed.

Lemma lencP (nc1 nc2 : natc) : reflect {subset nc1 <= nc2} (nc1 <= nc2).
Proof.
  apply: (iffP idP).
  - by move=> Hle m; rewrite !mem_ncP => /le_trans ->.
  - case: nc1 nc2 => [n1|] [n2|] //; first by apply; rewrite in_embedn.
    by move/(_ n2.+1)/implyP; rewrite in_embedn ltnn.
Qed.
Arguments lencP {nc1 nc2}.
Prenex Implicits lencP.

Lemma addnc0 (nc : natc) : nc `+ 0 = nc.
Proof. by case: nc => [?|] //=; rewrite addn0. Qed.

Lemma subnc0 (nc : natc) : nc `- 0 = nc.
Proof. by case: nc => [?|] //=; rewrite subn0. Qed.

Lemma addncnC (nc : natc) n1 n2 : nc `+ n1 `+ n2 = nc `+ n2 `+ n1.
Proof. by case: nc => [?|] //=; rewrite addnAC. Qed.

Lemma addncnA (nc : natc) n1 n2 : nc `+ (n1 + n2) = nc `+ n1 `+ n2.
Proof. by case: nc => [?|] //=; rewrite addnA. Qed.

Lemma addncnBAC (nc : natc) n1 n2 :
  n1 \in nc -> nc `- n1 `+ n2 = nc `+ n2 `- n1.
Proof. by case: nc => [?|] //= ?; rewrite addnC addnBA // addnC. Qed.

Lemma subncnK n (nc : natc) : n \in nc -> nc `- n `+ n = nc.
Proof. by case: nc => [?|] //=; rewrite in_embedn => ?; rewrite subnK. Qed.

Lemma subncnDA (nc : natc) n1 n2 : nc `- (n1 + n2) = nc `- n1 `- n2.
Proof. by case: nc => [?|] //=; rewrite subnDA. Qed.

Lemma lenc_addr (nc : natc) n : nc <= nc `+ n.
Proof. by case: nc => [?|] //=; apply: leq_addr. Qed.

Lemma lenc_add2r n : {mono addncn^~ n : nc1 nc2 / nc1 <= nc2}.
Proof. by move=> [?|] [?|] //=; apply: leq_add2r. Qed.

Lemma lenc_sub2r n : {homo subncn^~ n : nc1 nc2 / nc1 <= nc2}.
Proof. by move=> [?|] [?|] //=; apply: leq_sub2r. Qed.

Lemma lenc_subLRC (nc1 nc2 : natc) n : (nc1 `- n <= nc2) = (nc1 <= nc2 `+ n).
Proof.
  case: nc1 nc2 => [?|] [?|] //=.
  by rewrite addnC !lenc_embed !leEnat leq_subLR.
Qed.

Lemma mem_mcP m (mc : markingc) : (m \in mc) = (embedm m <= mc).
Proof. by apply: eq_forallb => p; rewrite ffunE mem_ncP. Qed.

Lemma embedm0 : embedm \bot = \bot :> markingc.
Proof. by apply/ffunP=> p; rewrite !ffunE. Qed.

Lemma embedm_add (m1 m2 : marking) : embedm (m1 :+: m2) = embedm m1 :`+: m2.
Proof. by apply/ffunP=> p; rewrite !ffunE. Qed.

Lemma embedm_sub (m1 m2 : marking) : embedm (m1 :-: m2) = embedm m1 :`-: m2.
Proof. by apply/ffunP=> p; rewrite !ffunE. Qed.

Lemma lemc_embed : {mono (embedm : marking -> markingc) : m1 m2 / m1 <= m2}.
Proof. by move=> m1 m2; apply: eq_forallb => p; rewrite !ffunE. Qed.

Lemma embedm_inj : injective (embedm : marking -> markingc).
Proof.
  by move=> m1 m2 H; apply/ffunP=> p; move/ffunP/(_ p): H; rewrite !ffunE; case.
Qed.

Lemma embedmP (mc : markingc) :
  reflect (exists m, mc = embedm m) [forall p, mc p != \top :> natc].
Proof.
  apply: (iffP forallP).
  - move=> neg_mcp_1.
    have /fin_all_exists [f H]: forall p : place, exists n : nat, mc p = embedn n.
      move=> p.
      move: (neg_mcp_1 p).
      case: (mc p) => // n _. by exists n.
    exists (finfun f). apply/ffunP=> p. by rewrite !ffunE.
  - move=> [m ->] p.
    by rewrite ffunE.
Qed.

(* Any markingc value is downward closed.  *)
Lemma mem_le_mc m1 m2 (mc : markingc) : m1 \in mc -> m2 <= m1 -> m2 \in mc.
Proof.
  by rewrite !mem_mcP => Hin Hle; rewrite (le_trans _ Hin) // lemc_embed.
Qed.

Lemma in_embedm (m1 m2 : marking) : (m1 \in embedm m2) = (m1 <= m2).
Proof. by rewrite mem_mcP lemc_embed. Qed.

Lemma in_0_mc (mc : markingc) : \bot \in mc.
Proof. by rewrite mem_mcP embedm0 le0x. Qed.

Definition inE := (in_top, in_embedn, in_0_nc, in_embedm, in_0_mc, inE).

Lemma lemcP (mc1 mc2 : markingc) : reflect {subset mc1 <= mc2} (mc1 <= mc2).
Proof.
  apply: (iffP idP).
  - by move=> Hle m; rewrite !mem_mcP => /le_trans ->.
  - move=> H; apply/forallP=> p; apply/lencP=> n H1.
    set m : marking := [ffun p' => if p' == p then n else 0].
    suff/forallP/(_ p) : m \in mc2 by rewrite ffunE eqxx.
    apply: H; apply/forallP=> p'; rewrite ffunE; case: eqP => [->|] //.
    by rewrite inE.
Qed.
Arguments lemcP {mc1 mc2}.
Prenex Implicits lemcP.

Lemma lemc_addr (mc : markingc) m : mc <= mc :`+: m.
Proof. by apply/forallP=> p; rewrite ffunE lenc_addr. Qed.

Lemma lemc_add2r (m : marking) : {mono addmcm^~ m : mc1 mc2 / mc1 <= mc2}.
Proof. by apply: relcw_fflift2r_mono => ? ? ?; rewrite lenc_add2r. Qed.

Lemma lemc_sub2r (m : marking) : {homo submcm^~ m : mc1 mc2 / mc1 <= mc2}.
Proof. by apply: relcw_fflift2r_homo => ? ? ? ?; rewrite lenc_sub2r. Qed.

Lemma lemc_subLRC (mc1 mc2 : markingc) m :
  (mc1 :`-: m <= mc2) = (mc1 <= mc2 :`+: m).
Proof. by apply: eq_forallb => p; rewrite !ffunE lenc_subLRC. Qed.

Lemma addmc0 (mc : markingc) : mc :`+: \bot = mc.
Proof. by apply/ffunP=> p; rewrite !ffunE addnc0. Qed.

Lemma submc0 (mc : markingc) : mc :`-: \bot = mc.
Proof. by apply/ffunP=> p; rewrite !ffunE subnc0. Qed.

Lemma addmcmC (mc : markingc) m1 m2 : mc :`+: m1 :`+: m2 = mc :`+: m2 :`+: m1.
Proof. by apply/ffunP=> p; rewrite !ffunE addncnC. Qed.

Lemma addmcmA (mc : markingc) m1 m2 : mc :`+: (m1 :+: m2) = mc :`+: m1 :`+: m2.
Proof. by apply/ffunP=> p; rewrite !ffunE addncnA. Qed.

Lemma addmcmBAC (mc : markingc) m1 m2 :
  m1 \in mc -> mc :`-: m1 :`+: m2 = mc :`+: m2 :`-: m1.
Proof. by move/forallP=> H; apply/ffunP=> p; rewrite !ffunE addncnBAC. Qed.

Lemma submcmK m (mc : markingc) : m \in mc -> mc :`-: m :`+: m = mc.
Proof. by move/forallP=> H; apply/ffunP=> p; rewrite !ffunE subncnK. Qed.

Lemma submcmDA (mc : markingc) m1 m2 : mc :`-: (m1 :+: m2) = mc :`-: m1 :`-: m2.
Proof. by apply/ffunP=> p; rewrite !ffunE subncnDA. Qed.

Lemma projmcT (mc : markingc) : projmc setT mc = mc.
Proof. by apply/ffunP=> ?; rewrite ffunE inE. Qed.

Lemma projmcI ps1 ps2 (mc : markingc) :
  projmc (ps1 :&: ps2) mc = projmc ps1 (projmc ps2 mc).
Proof. by apply/ffunP=> p; rewrite !ffunE inE; case: (p \in ps1). Qed.

Lemma lemc_proj (mc : markingc) ps : mc <= projmc ps mc.
Proof. by apply/forallP=> ?; rewrite ffunE; case: ifP. Qed.

Lemma projmc_add (mc : markingc) m ps :
  projmc ps (mc :`+: m) = projmc ps mc :`+: m.
Proof. by apply/ffunP=> ?; rewrite !ffunE; case: ifP. Qed.

Lemma projmc_sub (mc : markingc) m ps :
  projmc ps (mc :`-: m) = projmc ps mc :`-: m.
Proof. by apply/ffunP=> ?; rewrite !ffunE; case: ifP. Qed.

Lemma topsmc_embed (m : marking) : topsmc (embedm m) = set0.
Proof. by apply/setP=> p; rewrite !inE ffunE. Qed.

Lemma topsmc_add (m : marking) : {mono addmcm^~ m : mc / topsmc mc}.
Proof. by move=> mc; apply/setP=> p; rewrite !inE ffunE; case: (mc p). Qed.

Lemma topsmc_sub (m : marking) : {mono submcm^~ m : mc / topsmc mc}.
Proof. by move=> mc; apply/setP=> p; rewrite !inE ffunE; case: (mc p). Qed.

Lemma topsmc_proj ps (mc : markingc) : topsmc mc \subset topsmc (projmc ps mc).
Proof. by apply/subsetP=> p; rewrite !inE ffunE => /eqP->; rewrite if_same. Qed.

Lemma topsmc_proj_embed ps (m : marking) :
  topsmc (projmc ps (embedm m)) = ~: ps.
Proof. by apply/setP=> p; rewrite !inE /projmc !ffunE; case: ifP. Qed.

Variant nextmc_spec (mc : markingc) t : option markingc -> Prop :=
| NextmcSome of pre t \in mc
  : nextmc_spec mc t (Some (mc :`-: (pre t) :`+: (post t)))
| NextmcNone p of ~~ ((pre t) p \in mc p)
  : nextmc_spec mc t None.

Lemma nextmcP mc t : nextmc_spec mc t (nextmc mc t).
Proof.
  rewrite /nextmc; case: ifPn; first by move=> H; constructor.
  by rewrite negb_forall; move/existsP=> [p H]; constructor 2 with p.
Qed.

Lemma nextmc_proj t (ps : {set place}) :
  {homo projmc ps : mc1 mc2 / mc1 =1{` t `}> mc2}.
Proof.
  move=> mc1 mc2; case: nextmcP => // pre_t_in_mc1 [<-].
  rewrite /nextmc ifT; first by rewrite projmc_add projmc_sub.
  by apply/lemcP: pre_t_in_mc1; rewrite lemc_proj.
Qed.

Lemma topsmc_nextmc (mc1 mc2 : markingc) t :
  mc1 =1{` t `}> mc2 -> topsmc mc2 = topsmc mc1.
Proof. by case: nextmcP => // _ [<-]; rewrite topsmc_add topsmc_sub. Qed.

Lemma topsmc_nextmc_seq (mc1 mc2 : markingc) s :
  mc1 ={` s `}> mc2 -> topsmc mc2 = topsmc mc1.
Proof.
  elim/last_ind: s => [|s t IHs] in mc2 *; first by case=> <-.
  by rewrite foldm_rcons_some => -[mc3 [/IHs <-]]; apply: topsmc_nextmc.
Qed.

Lemma nextmc_seq_displacement (mc1 mc2 : markingc) s :
  mc1 ={` s `}> mc2 ->
  \summ_(t <- s) (pre t) \in mc1 :`+: \summ_(t <- s) (post t) /\
  mc2 = mc1 :`+: \summ_(t <- s) (post t) :`-: \summ_(t <- s) (pre t).
Proof.
  elim/last_ind: s => [| s t IHs] in mc2 *.
  - by case=> <-; rewrite !big_nil addmc0 submc0 inE.
  - rewrite foldm_rcons_some -!cats1 !big_cat !big_seq1 /= => -[mc3 [/IHs []]].
    set pres : marking := \summ_(t <- s) pre t.
    set posts : marking := \summ_(t <- s) post t.
    move=> Hpres ->; case: nextmcP => // Hpret [<-]; split.
    + rewrite mem_mcP addmC embedm_add addmcmA (le_trans _ (lemc_addr _ _)) //.
      by rewrite -(submcmK Hpres) lemc_add2r -mem_mcP.
    + by rewrite addmcmBAC // addmcmBAC // addmcmA submcmDA.
Qed.

Variant accelmc_spec (mcs : seq markingc) (mc : markingc) p
  : natc -> Prop :=
| AccelmcEmbednEmbedn n
    of forall (mc' : markingc), mc' \in mcs -> mc' <= mc -> ~~ (mc' p < mc p)
    & mc p = embedn n
  : accelmc_spec mcs mc p (embedn n)
| AccelmcEmbednTop n (mc' : markingc)
    of mc' \in mcs & mc' <= mc & mc' p < mc p & mc p = embedn n
  : accelmc_spec mcs mc p \top
| AccelmcTopTop of mc p = None
  : accelmc_spec mcs mc p \top.

Lemma accelmcP mcs mc p : accelmc_spec mcs mc p ((accelmc mcs mc) p).
Proof.
  case Hmc: (mc p) => [n|]; last by rewrite ffunE Hmc if_same; constructor.
  rewrite ffunE inE; case: ifP => /hasP /=.
  - rewrite Hmc => H; constructor=> //= mc' H1 H2.
    by apply/negP=> H'; apply: H; exists mc' => //; rewrite -Hmc H2 H'.
  - by move=> [mc' H] /andP [H1 H2]; apply: AccelmcEmbednTop H2 Hmc.
Qed.

Lemma topsmc_accel (mc :  markingc) mcs :
  topsmc mc \subset topsmc (accelmc mcs mc).
Proof. exact: topsmc_proj. Qed.

Variant nextma_spec (mcp : seq markingc * markingc) t :
  option (seq markingc * markingc) -> Type :=
| NextmaSome (mc : markingc) of (mcp.2 =1{` t `}> mc)
  : nextma_spec mcp t (Some (rcons mcp.1 mcp.2,
                             accelmc (rcons mcp.1 mcp.2) mc))
| NextmaNone of (nextmc mcp.2 t = None)
  : nextma_spec mcp t None.

Lemma lemc_accel mc (mcs : seq markingc) : mc <= accelmc mcs mc.
Proof. exact: lemc_proj. Qed.

Lemma nextmaP mcp t : nextma_spec mcp t (nextma mcp t).
Proof. by case H: (nextmc mcp.2 t) => [n|]; rewrite /nextma H; constructor. Qed.

Lemma topsmc_nextma mcs1 mcs2 (mc1 mc2 : markingc) t :
  (mcs1, mc1) =1{! t !}> (mcs2, mc2) -> topsmc mc1 \subset topsmc mc2.
Proof.
  by case: nextmaP => //= mc3 /topsmc_nextmc <- [-> <-]; apply: topsmc_accel.
Qed.

Lemma size_nextma_seq mcs1 (mc1 : markingc) mcs2 mc2 s :
  (mcs1, mc1) ={! s !}> (mcs2, mc2) -> size mcs2 = size mcs1 + size s.
Proof.
  elim/last_ind: s => [|s t IHs] in mcs2 mc2 *; first by rewrite addn0 => -[->].
  rewrite foldm_rcons_some size_rcons addnS => -[[mcs3 mc3] [/IHs <-]].
  by case: nextmaP => //= mc2' _ [<-]; rewrite size_rcons.
Qed.

Lemma nextma_seq_cat mcs1 (mc1 : markingc) mcs2 mc2 s1 s2 :
  (mcs1, mc1) ={! s1 ++ s2 !}> (mcs2, mc2) ->
  let: mcp := (take (size mcs1 + size s1) mcs2,
               nth mc1 (rcons mcs2 mc2) (size mcs1 + size s1)) in
  (mcs1, mc1) ={! s1 !}> mcp /\ mcp ={! s2 !}> (mcs2, mc2).
Proof.
  set n := size mcs1 + size s1.
  elim/last_ind: s2 => [|s2 t IHs2] in mcs2 mc2 *.
  - rewrite cats0 => Hmc1ps1.
    by rewrite /n -(size_nextma_seq Hmc1ps1) take_size nth_rcons ltnn eqxx.
  - rewrite -rcons_cat foldm_rcons_some => -[[mcs3 mc3] [Hmcp1s1s2 Hmcp3t]].
    move/(_ _ _ Hmcp1s1s2): IHs2 => [Hs1 Hs2].
    suff [-> ->]: take n mcs2 = take n mcs3 /\
                  nth mc1 (rcons mcs2 mc2) n = nth mc1 (rcons mcs3 mc3) n
      by split=> //; rewrite foldm_rcons_some; exists (mcs3, mc3).
    case: nextmaP Hmcp3t => //= mc2' Hmc3t [<- <-]; split.
    + rewrite -cats1 takel_cat //.
      by rewrite (size_nextma_seq Hmcp1s1s2) size_cat addnA leq_addr.
    + rewrite [LHS]nth_rcons ifT // size_rcons (size_nextma_seq Hmcp1s1s2).
      by rewrite ltnS size_cat addnA leq_addr.
Qed.

Lemma nextma_seq_split mc0 s mcs1 mc1 (mc : markingc) :
  ([::], mc0) ={! s !}> (mcs1, mc1) ->
  mc \in rcons mcs1 mc1 ->
  exists s1 s2 mcs, [/\ s = s1 ++ s2,
                     ([::], mc0) ={! s1 !}> (mcs, mc) &
                     (mcs, mc) ={! s2 !}> (mcs1, mc1)].
Proof.
  pose i := index mc mcs1.
  move=> Hmc0s Hinmcp1; move: (Hmc0s).
  rewrite -(cat_take_drop i s) => /nextma_seq_cat /=.
  rewrite add0n nth_rcons size_takel; last first.
  - by move: (size_nextma_seq Hmc0s); rewrite add0n => <-; rewrite index_size.
  - rewrite [if _ then _ else _](_ : _ = mc).
    + by move=> [Htake Hdrop]; exists (take i s), (drop i s), (take i mcs1).
    + move: Hinmcp1. rewrite mem_rcons inE.
      have: (i <= size mcs1)%N by rewrite index_size.
      rewrite leq_eqVlt index_mem.
      case: ifP; first by move/nth_index=> ->.
      by case: ifP => // _ _ _ /predU1P [].
Qed.

Lemma nextma_seq_extend (mcp1 : seq markingc * markingc) s mcp2 :
  mcp1 ={! s !}> mcp2 ->
  exists mcs, rcons mcp2.1 mcp2.2 = rcons mcp1.1 mcp1.2 ++ mcs.
Proof.
  elim/last_ind: s => [| s t IHs] in mcp2 *.
  - by move=> /= [<-]; exists [::]; rewrite cats0.
  - rewrite foldm_rcons_some => -[mcp [/IHs [mcs Hrcons] Ht]].
    exists (rcons mcs mcp2.2); rewrite -rcons_cat -Hrcons.
    apply/eqP; rewrite eqseq_rcons eqxx andbT.
    by case: nextmaP Ht => // mc _ [<-].
Qed.

Lemma nextma_seq_displacement mcs1 mcs2 (mc1 mc2 : markingc) s n2 p :
  (mcs1, mc1) ={! s !}> (mcs2, mc2) -> mc2 p = embedn n2 ->
  exists n1, [/\ mc1 p = embedn n1,
              \sum_(t <- s) (pre t) p <= n1 + \sum_(t <- s) (post t) p &
              n2 = n1 + \sum_(t <- s) (post t) p - \sum_(t <- s) (pre t) p].
Proof.
  elim/last_ind: s => [| s t IHs] in mcs2 mc2 n2 *.
  - by case=> _ <- ->; exists n2; rewrite !big_nil addn0 subn0.
  - rewrite foldm_rcons_some -!cats1 !big_cat !big_seq1 /=.
    move=> [[mcs3 mc3] [Hmcp1s Hmcp3t]] Hmc2p.
    case: nextmaP Hmcp3t Hmc2p => //= mc2'.
    case: nextmcP => // Hpret [<-] [_ <-].
    case: accelmcP => // n2' _; rewrite !ffunE.
    move/forallP/(_ p): Hpret; case Hmc3: (mc3 p) => [n3|] //=.
    have [n1] := IHs _ _ _ Hmcp1s Hmc3.
    set presp := \sum_(t0 <- s) (pre t0) p.
    set postsp := \sum_(t0 <- s) (post t0) p.
    rewrite inE => -[-> Hpresp ->] Hpret [<-] [<-]; exists n1.
    have Hprest : presp + (pre t) p <= n1 + (postsp + (post t) p).
    + rewrite addnC addnA leEnat (leq_trans _ (leq_addr _ _)) //.
      by rewrite -(subnK Hpresp) leq_add2r.
    + by split=> //; rewrite addnC addnBA // subnDA addnBA // addnC addnA.
Qed.

Lemma nextma_no_acc_nextmc mcs1 (mc1 : markingc) mcs2 mc2 t :
  (mcs1, mc1) =1{! t !}> (mcs2, mc2) -> topsmc mc2 \subset topsmc mc1
  -> mc1 =1{` t `}> mc2.
Proof.
  case: nextmaP => //= mc mc1_t_mc [] -> <- tops_sub.
  rewrite (_ : accelmc mcs2 mc = mc) //; apply/ffunP=> p.
  move/topsmc_nextmc : mc1_t_mc tops_sub => <- /subsetP /(_ p); rewrite !inE.
  by case: accelmcP => // _ _ _ _ _ _ /(_ isT) /eqP ->.
Qed.

Lemma nextma_no_acc_nextmc_seq mcs1 (mc1 : markingc) mcs2 mc2 s :
  (mcs1, mc1) ={! s !}> (mcs2, mc2) -> topsmc mc2 \subset topsmc mc1
  -> mc1 ={` s `}> mc2.
Proof.
  elim/last_ind: s => [|s t IHs] in mcs2 mc2 *; first by case=> _ ->.
  rewrite foldm_rcons_some => -[[mcs mc] [mcp1_s mcp_t]] tops_sub.
  move/IHs/(_ (subset_trans (topsmc_nextma mcp_t) tops_sub)): mcp1_s => mc1_s.
  apply/foldm_rcons_some; exists mc; split=> //.
  apply: nextma_no_acc_nextmc mcp_t _.
  by rewrite (subset_trans tops_sub) // (topsmc_nextmc_seq mc1_s).
Qed.

(* relations between nextm and nextmc *)

Lemma nextm_embed (m1 m2 : marking) t:
  m1 =1{ t }> m2 <-> embedm m1 =1{` t `}> embedm m2.
Proof.
  split.
  - case: nextmP => // Hpret [<-].
    by rewrite /nextmc inE Hpret embedm_add embedm_sub.
  - case: nextmcP => //; rewrite inE -embedm_sub -embedm_add /nextm => -> [].
    by move/embedm_inj=> <-.
Qed.

Lemma embedm_nextmc_nextm (m0 : marking) t mc1 : embedm m0 =1{` t `}> mc1 -> exists m1, mc1 = embedm m1 /\ m0 =1{ t }> m1.
Proof.
  case: nextmcP => //.
  rewrite inE -embedm_sub -embedm_add => /forallP pre_t_m0 [].
  exists (m0 :-: pre t :+: post t); split => //.
  case: nextmP => // p.
  by rewrite (pre_t_m0 p).
Qed.

(* Upward compatibility lemmas.  *)

Lemma nextm_ucompat_mem m1 m2 m1' t (mc1 mc2 : markingc) :
  m1 =1{ t }> m2 -> m1 <= m1' ->
  mc1 =1{` t `}> mc2 -> m1' \in mc1 ->
  exists m2',
    [/\ m1' =1{ t }> m2', m2 <= m2' & m2' \in mc2].
Proof.
  move=> Hm1t /submKC <- Hmc1t Hmc1'in.
  exists (m2 :+: (m1' :-: m1)); split; first by exact: nextm_add2r.
  - by rewrite lem_addr.
  - case: nextmcP Hmc1t Hmc1'in => // Hpretinmc1 [<-] Hm2'in.
    case: nextmP Hm1t => // Hpretp [<-].
    rewrite addmAC addmBAC // mem_mcP embedm_add embedm_sub.
    by rewrite lemc_add2r lemc_sub2r // -mem_mcP.
Qed.

Lemma nextmc_ucompat (mc1 mc2 mc1' : markingc) t :
  mc1 =1{` t `}> mc2 -> mc1 <= mc1' ->
  exists mc2',
    mc1' =1{` t `}> mc2' /\ mc2 <= mc2'.
Proof.
  case: nextmcP => // Hpret [<-] Hlemc1.
  exists (mc1' :`-: pre t :`+: post t); split.
  - by rewrite /nextmc ifT // (lemcP Hlemc1).
  - by rewrite lemc_add2r lemc_sub2r.
Qed.

Lemma nextmc_seq_ucompat (mc1 mc2 mc1' : markingc) s :
  mc1 ={` s `}> mc2 -> mc1 <= mc1' ->
  exists mc2',
    mc1' ={` s `}> mc2' /\ mc2 <= mc2'.
Proof.
  move=> H Hle; move: H.
  elim/last_ind: s => [|s t IHs] in mc2 *; first by case=> <-; exists mc1'.
  rewrite foldm_rcons_some => -[mc [Hmc1s Hmct]].
  case/(_ _ Hmc1s): IHs => mc' [Hmc1's Hlemc].
  have [mc2' [Hnextmc' Hlemc2]] := nextmc_ucompat Hmct Hlemc.
  by exists mc2'; split=> //; apply/foldm_rcons_some; exists mc'.
Qed.

Lemma nextmc_seq_ucompat_strict (mc1 mc2 mc1' : markingc) s :
  mc1 ={` s `}> mc2 -> mc1 <= mc1' ->
  exists mc2',
    [/\ mc1' ={` s `}> mc2', mc2 <= mc2' &
        forall p, mc1 p < mc1' p -> mc2 p < mc2' p].
Proof.
  move/(fun x => (x, x))=> [Hmc1s /nextmc_seq_displacement []].
  set pres : marking := \summ_(t <- s) pre t.
  set posts : marking := \summ_(t <- s) post t.
  move=> Hpresin -> Hlemc1.
  exists (mc1' :`+: posts :`-: pres); split.
  - have [mc2' [Hmc1's Hlemc2]] := nextmc_seq_ucompat Hmc1s Hlemc1.
    by have [_ <-] := nextmc_seq_displacement Hmc1's.
  - by rewrite lemc_sub2r // lemc_add2r.
  - move=> p; move/forallP/(_ p): Hpresin; rewrite !ffunE.
    case: (mc1 p) => [n1|] //; case: (mc1' p) => [n1'|] //.
    rewrite inE !ltnc_embed !ltEnat /= => Hpres /subnK <-.
    by rewrite -addSnnS -addnA -[X in (_ < X)%N]addnBA // addSnnS leq_addl.
Qed.

Lemma nextmc_seq_loop_ucompat (mc1 mc2 mc1' : markingc) s :
  mc1 ={` s `}> mc2 -> mc1 <= mc2 -> mc1 <= mc1' ->
  exists mc2',
    [/\ mc1' ={` s `}> mc2', mc1' <= mc2' & mc2 <= mc2'].
Proof.
  move/(fun x => (x, x))=> [Hmc1s /nextmc_seq_displacement []].
  set pres : marking := \summ_(t <- s) pre t.
  set posts : marking := \summ_(t <- s) post t.
  move=> Hpresin -> Hlemc1mc2 Hlemc1mc1'.
  exists (mc1' :`+: posts :`-: pres); split.
  - have [mc2' [Hmc1's Hlemc2]] := nextmc_seq_ucompat Hmc1s Hlemc1mc1'.
    by have [_ <-] := nextmc_seq_displacement Hmc1's.
  - apply/forallP=> p; move/forallP/(_ p): Hlemc1mc2.
    move/forallP/(_ p): Hlemc1mc1'; move/forallP/(_ p): Hpresin.
    rewrite !ffunE; case: (mc1' p) => [n1'|] //=; case: (mc1 p) => [n1|] //=.
    rewrite !inE !lenc_embed !leEnat => Hlepres /subnK <- Hlen1.
    by rewrite -addnA -addnBA // leq_add2l.
  - by rewrite lemc_sub2r // lemc_add2r.
Qed.

Lemma nextmc_seq_loop_unbounded (mc1 mc2 : markingc) s p :
  mc1 ={` s `}> mc2 -> mc1 <= mc2 -> mc1 p < mc2 p ->
  forall n, exists s' mc,
      [/\ mc1 ={` s' `}> mc, mc1 <= mc & n \in mc p].
Proof.
  move=> Hmc1s Hlemc1mc2 Hltmc1p.
  elim=> [|n IHn]; first by exists [::], mc1; rewrite inE.
  case: IHn => s' [mc3 [Hmc1s' Hlemc1mc3 Hmc3p]].
  have [mc4 [Hmc2s' Hlemc3 /(_ _ Hltmc1p) Hltmc3p]] :=
    nextmc_seq_ucompat_strict Hmc1s' Hlemc1mc2.
  exists (s ++ s'), mc4; split; first by rewrite foldm_cat_some; exists mc2.
  - exact: le_trans Hlemc3.
  - move: Hmc3p Hltmc3p; case: (mc4 p) => [n4|] //; case: (mc3 p) => [n3|] //.
    by rewrite !inE ltnc_embed ltEnat; apply: leq_ltn_trans.
Qed.

Lemma nextmc_seq_loop_consolidate (mc1 mc : markingc) :
  (forall p, exists s mc2,
        [/\ mc1 ={` s `}> mc2, mc1 <= mc2 & mc p <= mc2 p]) ->
  exists s mc2,
    [/\ mc1 ={` s `}> mc2, mc1 <= mc2 & mc <= mc2].
Proof.
  move=> Hp.
  suff: forall ps : seq place,
      exists s mc2, [/\ mc1 ={` s `}> mc2, mc1 <= mc2 & forall p,
                         p \in ps -> mc p <= mc2 p].
  { move/(_ (enum place))=> [s [mc2 [Hmc1s Hlemc1 Hmcp]]]; exists s, mc2.
    by split=> //; apply/forallP=> p; apply: Hmcp; rewrite enumT. }
  elim=> [|p ps [s [mc2 [IHmc1s IHlemc1 IHmcp]]]]; first by exists [::], mc1.
  have [s' [mc3 [Hmc1s' Hlemc1 Hmcp]]] := Hp p.
  have [mc4 [Hmc2s' Hlemc2 Hlemc3]] :=
    nextmc_seq_loop_ucompat Hmc1s' Hlemc1 IHlemc1.
  exists (s ++ s'), mc4; split; first by apply/foldm_cat_some; exists mc2.
  - by apply: le_trans Hlemc3.
  - move=> p'; rewrite inE => /predU1P [->|/IHmcp Hlemcp'].
    + by move/forallP/(_ p): Hlemc3 => /(le_trans _) ->.
    + by move/forallP/(_ p'): Hlemc2 => /(le_trans _) ->.
Qed.

(* Simulating transitions with accelerations with those without them.  *)

Lemma nextma_seq_simulatable (mc1 mc2 : markingc) mcs1 mcs2 s mc1' :
  (mcs1, mc1) ={! s !}> (mcs2, mc2) ->
  mc1 <= mc1' -> topsmc mc2 \subset topsmc mc1' ->
  exists mc2',
    mc1' ={` s `}> mc2' /\ mc2 <= mc2'.
Proof.
  elim/last_ind: s => [| s t IHs] in mcs2 mc2 *.
  - by case=> _ <- ? ?; exists mc1'.
  - rewrite foldm_rcons_some => -[[mcs3 mc3] [Hmcp1s Hmcp3t]] Hlemc1 Hsub.
    case: (IHs _ _ Hmcp1s) => // [| mc3' [Hmc1's Hlemc3]].
    + by apply: subset_trans Hsub; apply: topsmc_nextma Hmcp3t.
    + case: nextmaP Hmcp3t Hsub => //= [mc4 Hmc3t [-> <-]] Hsub.
      have [mc4' [Hmc3't Hlemc4]] := nextmc_ucompat Hmc3t Hlemc3.
      exists mc4'; rewrite foldm_rcons_some; split; first by exists mc3'.
      apply/forallP=> p; move/subsetP/(_ p): Hsub; rewrite !inE; case: accelmcP.
      * by move=> n4 _ <-; move/forallP: Hlemc4.
      * move=> _ _ _ _ _ _ /(_ isT) Hmc1'p.
        move/topsmc_nextmc: Hmc3't.
        move/topsmc_nextmc_seq: Hmc1's => -> /setP/(_ p).
        by rewrite !inE le1x => ->.
      * by move=> Hmc4p _; move/forallP/(_ p): Hlemc4; rewrite Hmc4p.
Qed.

Lemma nextma_seq_loop_simulatable mcs1 mcs2 (mc1 mc2 mc3 mc1' : markingc) s t p :
  (mcs1, mc1) ={! s !}> (mcs2, mc2) -> mc2 =1{` t `}> mc3 ->
  mc1 <= mc3 -> mc1 p < mc3 p ->
  mc1 <= mc1' -> topsmc mc2 \subset topsmc mc1' -> mc1' p <> \top ->
  exists mc3',
    [/\ mc1' ={` rcons s t `}> mc3', mc1' <= mc3' & mc1' p < mc3' p].
Proof.
  move=> Hmc1s Hmc2t Hlemc1 Hltmc1 Hmc1' Hsub Hnottop.
  pose prest : marking := \summ_(t <- rcons s t) (pre t).
  pose postst : marking := \summ_(t <- rcons s t) (post t).
  have Hmc3d p' n3 : mc3 p' = embedn n3 ->
                     exists n1, [/\ mc1 p' = embedn n1,
                                 prest p' <= n1 + postst p' &
                                 n3 = n1 + postst p' - prest p'].
  { case: nextmcP Hmc2t => // Hpret [<-]; rewrite !ffunE !summE.
    move=> Hmc3; move/forallP/(_ p'): Hpret; move: Hmc3.
    case Hmc2p': (mc2 p') => [n2|] //= [<-].
    rewrite inE -!cats1 !big_cat !big_seq1 /= => Hpretp'.
    have [n1 H] := nextma_seq_displacement Hmc1s Hmc2p'.
    move: H Hpretp'.
    set presp' := \sum_(t <- s) (pre t) p'.
    set postsp' := \sum_(t <- s) (post t) p'.
    move=> [Hmc1p' Hpresp' ->]; exists n1.
    have Hn1st: presp' + (pre t) p' <= n1 + (postsp' + (post t) p').
    - rewrite addnC addnA leEnat (leq_trans _ (leq_addr _ _)) //.
      by rewrite -(subnK Hpresp') leq_add2r.
    - by split=> //; rewrite addnC addnBA // subnDA addnBA // addnC addnA. }
  have [mc2' [Hmc1's Hlemc2]] := nextma_seq_simulatable Hmc1s Hmc1' Hsub.
  have [mc3' [Hmc3't Hlemc3]] := nextmc_ucompat Hmc2t Hlemc2.
  have Hmc1'st : mc1' ={` rcons s t `}> mc3'
    by rewrite foldm_rcons_some; exists mc2'.
  have Hmc3'd p': prest p' \in mc1' p' `+ postst p' /\
                  mc3' p' = mc1' p' `+ postst p' `- prest p'.
  { have [/forallP/(_ p') H1 /ffunP/(_ p') H2] :=
      nextmc_seq_displacement Hmc1'st.
    by rewrite !ffunE in H1 H2. }
  exists mc3'; split=> //.
  - apply/forallP=> p'; move/forallP/(_ p'): Hlemc3.
    move/(_ p'): Hmc3'd => [Hprestp' ->]; move/forallP/(_ p'): Hmc1'.
    case: (mc1' p') Hprestp' => [n1'|] //=; move/forallP/(_ p'): Hlemc1.
    move/(_ p'): Hmc3d.
    case: (mc3 p') => [n3'|] //=; rewrite inE !lenc_embed.
    move/(_ _ erefl) => [n1]; case: (mc1 p') => [n1''|] //= [->].
    rewrite !lenc_embed !leEnat.
    move=> Hprestn1 -> Hlen1n3 Hprestn1' Hlen1n1' Hlen3n3'.
    by rewrite -(subnK Hlen1n1') -addnA -addnBA // leq_add2l.
  - move/forallP/(_ p): Hlemc3.
    move/(_ p): Hmc3'd => [Hprestp ->]; move/forallP/(_ p): Hmc1'.
    case: (mc1' p) Hprestp Hnottop => [n1'|] //=; move/(_ p): Hmc3d Hltmc1.
    case: (mc3 p) => [n3'|] //=; rewrite inE !lenc_embed.
    move/(_ _ erefl) => [n1]; case: (mc1 p) => [n1''|] //= [->].
    rewrite !lenc_embed !ltnc_embed !leEnat !ltEnat /=.
    move=> Hprestn1 -> Hlen1n3 Hprestn1' _ Hlen1n1' Hlen3n3'.
    by rewrite -(subnK Hlen1n1') -addnA -addnBA // ltn_add2l.
Qed.

(* Soundness lemmas with respect to coverability.  *)

Lemma nextmc_cover_sound (mc1 mc2 : markingc) m t :
  mc1 =1{` t `}> mc2 -> m \in mc2 ->
  exists m1 m2,
    [/\ m1 =1{ t }> m2, m1 \in mc1, m2 \in mc2 & m <= m2].
Proof.
  case: nextmcP => // Hpret [<-] Hm.
  pose m1 := m :-: post t :+: pre t.
  pose m2 := m1 :-: pre t :+: post t.
  exists m1, m2.
  have Hin : m1 \in mc1.
  { rewrite mem_mcP embedm_add -(submcmK Hpret) lemc_add2r.
    by rewrite embedm_sub lemc_subLRC -mem_mcP. }
  split=> //.
  - by case: nextmP => // p /negP []; rewrite !ffunE leEnat leq_addl.
  - by rewrite mem_mcP embedm_add lemc_add2r embedm_sub lemc_sub2r -?mem_mcP.
  - by rewrite /m2 addmK -lem_subLRC.
Qed.

Lemma nextmc_seq_cover_sound (mc1 mc2 : markingc) m s :
  mc1 ={` s `}> mc2 -> m \in mc2 ->
  exists m1 m2,
    [/\ m1 ={ s }> m2, m1 \in mc1, m2 \in mc2 & m <= m2].
Proof.
  elim/last_ind: s => [|s t IHs] in m mc2 *.
  - by rewrite /= => -[<-] Hm; exists m, m.
  - rewrite foldm_rcons_some => -[mc3 [Hmc1s Hmc3t]] Hmin.
    have [m3' [m2' [Hm3't Hm3'in Hm2'in Hlem]]] :=
      nextmc_cover_sound Hmc3t Hmin.
    have [m1 [m3 [Hm1s Hm1in Hm3in Hlem3']]] := IHs _ _ Hmc1s Hm3'in.
    have [m2 [Hm3t Hlem2' Hm2in]] := nextm_ucompat_mem Hm3't Hlem3' Hmc3t Hm3in.
    exists m1, m2; split=> //; last by apply: le_trans Hlem2'.
    by rewrite foldm_rcons_some; exists m3.
Qed.

Lemma accelmc_cover_sound_mc m0 s mcs1 mc1 t (mc : markingc) mc2 :
  ([::], embedm m0) ={! s !}> (mcs1, mc1) ->
  mc1 =1{` t `}> mc2 ->
  mc <= accelmc (rcons mcs1 mc1) mc2 ->
  topsmc mc \subset topsmc mc2 ->
  exists s' (mc' : markingc),
    [/\ mc2 ={` s' `}> mc', mc2 <= mc' & mc <= mc'].
Proof.
  move=> Hm0s Hmc1t Hlemc Hsubmcmc2; apply: nextmc_seq_loop_consolidate => p.
  case: nextmcP (Hmc1t) => // Hpret [] Hmc2.
  move/forallP/(_ p): Hlemc. case: accelmcP.
  - by move=> n2 _ <- Hlemcp; exists [::], mc2.
  - move=> n2 mc3 Hinmc1p Hlemc3 Hltmc3p Hmc2p _.
    have [s1 [s2 [mcs3 [Hs1s2 Hm0s1 Hmcp3s2]]]] :=
      nextma_seq_split Hm0s Hinmc1p.
    have Hsub: topsmc mc1 \subset topsmc mc2
      by rewrite (topsmc_nextmc Hmc1t).
    have Hnottop: mc2 p <> \top by rewrite Hmc2p.
    have [mc4 [Hmc2s2t Hlemc2 Hltmc2p]] :=
      nextma_seq_loop_simulatable Hmcp3s2 Hmc1t Hlemc3 Hltmc3p Hlemc3 Hsub
                                  Hnottop.
    have [n ->] : exists n, mc p = embedn n.
    { move/subsetP/(_ p): Hsubmcmc2 Hmc2p; rewrite !inE.
      case: (mc2 p) => [n2'|] //; case: (mc p) => [n _ _|/implyP] //.
      by exists n. }
    have [s' [mc' [Hmc2iter Hlemc2mc' Hlemc'p]]] :=
      nextmc_seq_loop_unbounded Hmc2s2t Hlemc2 Hltmc2p n.
    by exists s', mc'; rewrite -mem_ncP.
  - by move=> Hmc2top _; exists [::], mc2; rewrite Hmc2top.
Qed.

Lemma accelmc_cover_sound (m0 : marking) s mcs1 mc1 t m mc2 :
  ([::], embedm m0) ={! s !}> (mcs1, mc1) ->
  mc1 =1{` t `}> mc2 ->
  m \in accelmc (rcons mcs1 mc1) mc2 ->
  exists s' m2 m',
    [/\ m2 ={ s' }> m', m2 \in mc2 & m <= m'].
Proof.
  move=> Hm0s Hmc1t Hm.
  set mc := embedm m.
  have Hmin : m \in mc by rewrite inE.
  have Hmc : mc <= accelmc (rcons mcs1 mc1) mc2 by rewrite -mem_mcP.
  have Hsub: topsmc mc \subset topsmc mc2
    by apply/subsetP=> p; rewrite topsmc_embed inE.
  have [s' [mc' [Hmc2s' Hlemc2 Hlemc]]] :=
    accelmc_cover_sound_mc Hm0s Hmc1t Hmc Hsub.
  move/lemcP/(_ _ Hmin): Hlemc => Hminmc'.
  have [m2 [m' [Hm2s' Hm2in Hm'in Hlem]]] :=
    nextmc_seq_cover_sound Hmc2s' Hminmc'.
  by exists s', m2, m'.
Qed.

Theorem nextma_seq_cover_sound (m0 : marking) s mcs mc :
  ([::], embedm m0) ={! s !}> (mcs, mc) ->
  forall m, m \in mc -> exists s' m', m0 ={ s' }> m' /\ m <= m'.
Proof.
  elim/last_ind: s =>[|s t IHs] in mcs mc * .
  - by case=> _ <- m; rewrite inE; exists [::], m0.
  - rewrite foldm_rcons_some => -[[mcs1 mc1] [Hs]].
    case: nextmaP => //= mc2 Hmc1t [] _ {mcs} <- m Hacc.
    have [s2 [m2' [m' [Hm2's2 Hm2'in Hlem]]]] :=
      accelmc_cover_sound Hs Hmc1t Hacc.
    have [m1'' [m2'' [Hm1''t Hm1''in Hm2''in Hlem2']]] :=
      nextmc_cover_sound Hmc1t Hm2'in.
    have [s1 [m1 [Hm0s1 Hlem1'']]] := IHs _ _ Hs _ Hm1''in.
    have [m2 [Hm1t Hlem2'']] := nextm_ucompat Hm1''t Hlem1''.
    have [m3 [Hm2s2 Hlem']] := nextm_seq_ucompat Hm2's2
                                                 (le_trans Hlem2' Hlem2'').
    exists (rcons s1 t ++ s2), m3; split; last by apply: le_trans Hlem'.
    rewrite foldm_cat_some; exists m2; split=> //.
    by rewrite foldm_rcons_some; exists m1.
Qed.

(* Completeness lemmas with respect to coverability.  *)

Lemma nextma_seq_proj_cover_complete (m0 m : marking) s :
  m0 ={ s }> m -> exists ps, let mc := projmc ps (embedm m) in exists s' mcs,
                       ([::], embedm m0) ={! s' !}> (mcs, mc) /\
                       uniq (rcons mcs mc).
Proof.
  elim/last_ind: s => [|s t IHs] in m *.
  - by case=> <-; exists setT, [::], [::]; rewrite projmcT.
  - rewrite foldm_rcons_some => -[m1 [m0_s m1_t]].
    case/IHs: m0_s => ps [s1 [mcs1 [m0_s1 uniq_mcs1mc1]]].
    set mc1 := projmc ps _ in m0_s1 uniq_mcs1mc1.
    move/nextm_embed/(nextmc_proj ps): m1_t; rewrite -/mc1.
    case mcs1mc1_t: (_ (mcs1, mc1) t) / nextmaP => /= [mc' mc1_t_mc'|-> //].
    move=> mc1_t; move: mc1_t mc1_t_mc' mcs1mc1_t => -> [<-] {mc'}.
    rewrite /accelmc -projmcI; set ps2 := _ :&: ps => mcs1mc1_t; exists ps2.
    set mc2 := projmc ps2 _.
    have [| mc2_notin_mcs1mc1] := boolP (mc2 \in (rcons mcs1 mc1)).
    + case/(nextma_seq_split m0_s1) => s2 [s3 [mcs2 [_ m0_s2 mc_s3]]].
      exists s2, mcs2; split=> //.
      case/nextma_seq_extend: mc_s3 uniq_mcs1mc1 => mcs3 /= ->.
      by rewrite cat_uniq => /andP [].
    + exists (rcons s1 t), (rcons mcs1 mc1); rewrite foldm_rcons_some.
      split; first by exists (mcs1, mc1).
      by rewrite rcons_uniq mc2_notin_mcs1mc1.
Qed.

Lemma nextma_seq_cover_complete (m0 m : marking) s :
  m0 ={ s }> m -> exists mc : markingc,  (m \in mc) /\ exists s' mcs,
                       ([::], embedm m0) ={! s' !}> (mcs, mc) /\
                       uniq (rcons mcs mc).
Proof.
  case/nextma_seq_proj_cover_complete=> ps mc_reachable.
  by exists (projmc ps (embedm m)); rewrite mem_mcP lemc_proj.
Qed.

End Theory.

Arguments lencP {nc1 nc2}.
Prenex Implicits lencP.
Arguments lemcP {pn mc1 mc2}.
Prenex Implicits lemcP.
